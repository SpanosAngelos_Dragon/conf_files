"Configuration file for the Vim Editor

" ***** Built-in Configuration Options ***** "
source ~/.vim/plugins/main.vim

"Global Variables
source ~/.vim/globals/main.vim

"Edits
source ~/.vim/write/main.vim

"Cursor
source ~/.vim/cursor/main.vim

"Syntax
source ~/.vim/read/main.vim

"Browse
source ~/.vim/browse/main.vim
