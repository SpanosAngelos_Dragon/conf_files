#! /bin/bash

outputDirectory=../
pdfViewer=evince
main='Product'
pdflatex -output-directory=$outputDirectory $main".tex"
rm $outputDirectory/$main".out"
rm $outputDirectory/*".aux"
rm $outputDirectory/$main".log"
rm $outputDirectory/$main".toc"
$pdfViewer $outputDirectory/$main".pdf" &
