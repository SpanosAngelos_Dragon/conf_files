"Source the commands for the tab options
source ~/.vim/write/tab.vim

"Source commands for searching
source ~/.vim/write/search.vim

"Source indentation commands
source ~/.vim/write/indent.vim
