"Tab length
set tabstop=4

"Usually we prefer to expand the tab to prote
"ourselves from getting mismatches when we 
"open the same file in different editors.
set expandtab

"In Makefile we preserve the tab
"since it has indent driven syntax.
autocmd BufEnter Makefile       :set noexpandtab
autocmd BufEnter Makefile.rules :set noexpandtab
