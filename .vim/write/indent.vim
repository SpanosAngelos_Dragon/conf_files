"Set autoindentation
set autoindent

"On log and txt files we don't want autoindent
autocmd BufEnter *.log :set noautoindent
"autocmd BufLeave *.log :set autoindent
autocmd BufEnter *.txt :set noautoindent
"autocmd BufLeave *.txt :set autoindent
