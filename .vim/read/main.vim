"Source syntax commands
source ~/.vim/read/syntax.vim

"Source wrap commands
source ~/.vim/read/wrap.vim

"Source line enumeration coommands
source ~/.vim/read/lineNumeration.vim
