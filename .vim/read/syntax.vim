" ****** Script for loading custom highlight syntax files ****** "
source ~/.vim/syntax/git_commit_syntax.vim


"Enable Syntax only if it is supported
if &t_Co > 1
   syntax enable
endif

autocmd BufEnter,BufNew Makefile.rules :set filetype=make
autocmd BufEnter,BufNew makefile*rules :set filetype=make
