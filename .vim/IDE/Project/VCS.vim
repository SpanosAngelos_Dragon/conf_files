" ===========================================================================
"  File          :    VCS.vim
"  Entity        :    VCS
"  Purpose       :    VCS Class
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 17-Jul-2013
" ===========================================================================
let VCS = {}
let VCS.xml = {}

function! VCS.VCS ( ... )

   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:vcs = copy ( self )
      let l:vcs.xml = XML.XML ( 'BUFFER' )
      return l:vcs
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:vcs = copy ( self )
      let l:vcs.xml = XML.XML ( 'BUFFER' )
      return l:vcs
   else
      let l:vcs = copy( self )
      let l:vcs.xml [ 'Name' ]    = a:1
      let l:vcs.xml [ 'Address' ] = a:2
      return l:vcs
   endif

endfunction

function! VCS.GetFromUser ( )
   
   let l:vcs = copy ( self )
   let l:vcs.xml [ 'Name' ]    = input ( 'Enter Version Control program name.' )
   let l:vcs.xml [ 'Address' ] = input ( 'Enter Version Control System address.' )
   return l:vcs

endfunction
