" ===========================================================================
"  File          :    WebHost.vim
"  Entity        :    Project
"  Purpose       :    WebHost Class
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 17-Jul-2013
" ===========================================================================
let WebHost = {}
let WebHost.xml = {}

function! WebHost.WebHost ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:webHost = copy ( self )
      let l:webHost.xml = XML.XML ( 'BUFFER' )
      return l:webHost
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:webHost = copy ( self )
      let l:webHost.xml = XML.XML ( 'BUFFER' )
      return l:webHost
   else
      let l:webHost = copy ( self )
      let l:webHost.xml [ 'Home' ] = a:1
      let l:webHost.xml [ 'Wiki' ] = a:2
      return l:webHost
   endif

endfunction

function! WebHost.GetFromUser ( )
   
   let l:webHost = copy ( self )
   let l:webHost.xml [ 'Home' ] = input ( 'Please enter home address.' )
   let l:webHost.xml [ 'Wiki' ] = input ( 'Please enter wiki address.' )
   return l:webHost

endfunction
