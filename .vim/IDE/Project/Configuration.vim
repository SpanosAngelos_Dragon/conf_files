" ===========================================================================
"  File          :    Configuration.vim
"  Entity        :    IDE
"  Purpose       :    Configuration Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Configuration = { }
let Configuration.xml = { }

function! Configuration.Configuration ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:configuration = copy ( self )
      let l:configuration.xml = XML.XML ( 'BUFFER' )
      return l:configuration
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:configuration = copy ( self )
      let l:configuration.xml = XML.XML ( 'BUFFER' )
      return l:configuration
   endif

endfunction

function! Configuration.GetFromUser ( )

   let l:configuration = copy ( self )
   let l:configuration.xml [ 'Name'    ] = input ( 'Please enter configuration  name.' )
   let l:configuration.xml [ 'Date'    ] = strftime ( "%d/%m/%y" )
   let l:configuration.xml [ 'File'    ] = system ( 'find ./ -name "*.proj" )
   let l:configuration.xml [ 'VCS'     ] = VCS.VCS         ( 'USER' )
   let l:configuration.xml [ 'WebHost' ] = WebHost.WebHost ( 'USER' )
   return l:configuration

endfunction
