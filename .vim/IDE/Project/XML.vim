" ===========================================================================
"  File          :    XML.vim  
"  Entity        :    IDE
"  Purpose       :    XML Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let XML  = {}

function! XML.XML ( ... )

   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'BUFFER' )
      return self.GetFromBuffer ( { } )
   elseif ( a:1 == 'FILE' )
      execute ' silent edit '
      return self.GetFromBuffer ( { } )
   endif

endfunction

function! XML.GetFromBuffer ( )

   let l:xmlTag = matchstr ( l:line , "^ *\zs.*\ze *{" )
   let l:xml[ l:xmlTag ] = [ ]

   cursor ( line ( '.' ) + 1, 0 )
   while !self.IsEndOfSection ( )
      if ( self.IsStartOfSection ( ) )
         call insert ( l:xml [ l:xmlTag ], XML.GetFromBuffer ( ) )
      elseif ( self.IsEmptyLine ( ) )
         cursor ( line ( '.' ) + 1, 0 )
      else
         call insert ( l:xml [ l:xmlTag ], matchstr ( l:line '^ *\zs.*\ze *$' ) )
         cursor ( line ( '.' ) + 1, 0 )
      endif
   endwhile
   return l:xml

endfunction

function! XML.SetBuffer ( ... )

   if ( a:0 == 0)
      let l:xml = self
      let l:xmlIndent = ""
   else
      let l:xml = a:1
      let l:xmlIndent = a:2
   endif

   for l:xmlKey in keys ( l:xml )
      setline ( line ( '.' ), l:xmlIndent . l:xmlKey . "\n" )
      cursor ( line ( '.' ) + 1, 0 )
      for l:xmlContent in l:xml [ l:xmlKey ]
         if ( type ( l:xmlContent ) = type ( { } ) )
            self.setBuffer ( l:xmlContent, l:xmlIndent . "  " )
         else
            setline ( line ( '.' ), l:xmlIndent . l:xmlContent . "\n" )
            cursor ( line ( '.' ) + 1, 0 )
         endif
      end
      setline ( line ( '.' ), l:xmlIndent . '}' . "\n" )
      cursor ( line ( '.' ) + 1, 0 )
   end

endfunction

function! XML.IsEmptyFile( )
    return (line( '.' ) == 1)
endfunction

function! XML.IsEmptyLine ( )
   return empty ( matchstr ( l:line, '^ *$' ) )
endfunction

function! XML.IsEndOfSection ( )
   return !empty( matchstr( getline ( '.' ), '}' ) )
endfunction

function! XML.IsStartOfSection ( )
   return !empty( matchstr( getline ( '.' ), '{' ) )
endfunction
