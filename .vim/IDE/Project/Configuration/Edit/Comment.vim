" ===========================================================================
"  File          :    Comment.vim
"  Entity        :    IDE
"  Purpose       :    Comment Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Comment     = {}
let Comment.xml = {}

function! Comment.Comment ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'BUFFER' )
      let l:plugin = copy ( self )
      let l:plugin.xml = XML.XML ( 'BUFFER' )
      return l:plugin
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:plugin = copy ( self )
      let l:plugin.xml = XML.XML ( 'BUFFER' )
      return l:plugin
   endif

endfunction
