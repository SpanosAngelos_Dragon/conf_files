" ===========================================================================
"  File          :    Abbreviation.vim
"  Entity        :    IDE
"  Purpose       :    Abbreviation Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Abbreviation     = {}
let Abbreviation.xml = {}

function! Abbreviation.Abbreviation ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:abbreviation = copy ( self )
      let l:abbreviation.xml = XML.XML ( 'BUFFER' )
      return l:abbreviation
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:abbreviation = copy ( self )
      let l:abbreviation.xml = XML.XML ( 'BUFFER' )
      return l:abbreviation
   endif

endfunction

function! Abbreviation.GetFromUser ( )

   let l:abbreviation = copy ( self )
   let l:key = input ( 'Please enter abbreviation key.' )
   let l:abbreviation.xml [ l:key ] = input ( 'Please enter abbreviation extension.' )
   return l:abbreviation

endfunction


