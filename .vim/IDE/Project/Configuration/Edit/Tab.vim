" ===========================================================================
"  File          :    Tab.vim
"  Entity        :    IDE
"  Purpose       :    Tab Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Tab     = {}
let Tab.xml = {}

function! Tab.Tab ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:tab = copy ( self )
      let l:tab.xml = XML.XML ( 'BUFFER' )
      return l:tab
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:tab = copy ( self )
      let l:tab.xml = XML.XML ( 'BUFFER' )
      return l:tab
   endif

endfunction

function! Tab.GetFromUser ( )

   let l:tab = copy ( self )
   let l:tab.xml [ 'Space'     ] = input ( 'Please enter tab spacing.' )
   let l:tab.xml [ 'Character' ] = input ( 'Please enter tab character.' )
   return l:tab

endfunction

