" ===========================================================================
"  File          :    Plugin.vim
"  Entity        :    IDE
"  Purpose       :    Plugin Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Plugin     = {}
let Plugin.xml = {}

function! Plugin.Plugin ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:plugin = copy ( self )
      let l:plugin.xml = XML.XML ( 'BUFFER' )
      return l:plugin
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:plugin = copy ( self )
      let l:plugin.xml = XML.XML ( 'BUFFER' )
      return l:plugin
   endif

endfunction

function! Plugin.GetFromUser ( )

   let l:plugin = copy ( self )
   let l:namePropmt = 'Please enter plugin name. ( Press enter for return. )'

   let l:name =  input ( l:propmt )
   while ( ! empty ( l:name ) ) {
      let l:plugin.xml [ l:key ] = input ( 'Please enter plugin extension.' )
      let l:name = input ( l:propmt )
   endwhile
   return l:plugin

endfunction



