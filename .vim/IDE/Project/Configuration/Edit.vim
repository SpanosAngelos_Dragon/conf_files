" ===========================================================================
"  File          :    Edit.vim
"  Entity        :    IDE
"  Purpose       :    Edit Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Edit = { }
let Edit.xml = { }

function! Edit.Edit ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:edit = copy ( self )
      let l:edit.xml = XML.XML ( 'BUFFER' )
      return l:edit
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:edit = copy ( self )
      let l:edit.xml = XML.XML ( 'BUFFER' )
      return l:edit
   endif

endfunction

function! Edit.GetFromUser ( )

   let l:edit = copy ( self )
   let l:edit.xml [ 'Tab' ] = Tab.Tab ( 'USER' )
   return l:edit

endfunction
