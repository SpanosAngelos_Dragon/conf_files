" ===========================================================================
"  File          :    License.vim
"  Entity        :    IDE
"  Purpose       :    License class
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 19-Jul-2013
" ===========================================================================
let License = { }
let License.xml = { }

function! License.License ( ... )

   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:license = copy ( self )
      let l:license.xml = XML.XML ( 'BUFFER' )
      return l:license
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:license = copy ( self )
      let l:license.xml = XML.XML ( 'BUFFER' )
      return l:license
   else
      let l:license = copy ( self )
      let l:license.xml [ 'FileName'] = a:1
      let l:license.xml [ 'Version' ] = a:2
      return l:license
   endif

endfunction

function! License.GetFromUser ( )

   let l:license = copy ( self );
   let l:license.xml [ 'FileName' ] = input ( 'Please enter the file-name of the license.' )
   let l:license.xml [ 'Version'  ] = input ( 'Please enter the version of the license.' )
   return l:license

endfunction
