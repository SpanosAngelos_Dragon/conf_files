" ===========================================================================
"  File          :    Tool.vim
"  Entity        :    IDE
"  Purpose       :    Tool class
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 19-Jul-2013
" ===========================================================================
let Tool = { }
let Tool.xml = { }

function! Tool.Tool ( ... )

   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:tool = copy ( self )
      let l:tool.xml = XML.XML ( 'BUFFER' )
      return l:tool
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:tool = copy ( self )
      let l:tool.xml = XML.XML ( 'BUFFER' )
      return l:tool
   else
      let l:tool = copy ( self )
      let l:tool.xml [ 'Name'    ] = a:1
      let l:tool.xml [ 'Path'    ] = a:2
      let l:tool.xml [ 'Flags'   ] = a:3
      let l:tool.xml [ 'License' ] = License.License ( a:4[ 0 ], a:4[ 1 ] )
      return l:tool
   endif

endfunction
