" ===========================================================================
"  File          :    Project.vim
"  Entity        :    IDE
"  Purpose       :    Project Class.
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 16-Jul-2013
" ===========================================================================
let Project     = {}
let Project.xml = {}

function! Project.Project ( ... )
   
   if     ( a:1 == 'NULL' )
      return copy ( self )
   elseif ( a:1 == 'USER' )
      return self.GetFromUser ( )
   elseif ( a:1 == 'BUFFER' )
      let l:project = copy ( self )
      let l:project.xml = XML.XML ( 'BUFFER' )
      return l:project
   elseif ( a:1 == 'FILE' )
      execute 'silent edit' . a:1
      let l:project = copy ( self )
      let l:project.xml = XML.XML ( 'BUFFER' )
      return l:project
   else
      let l:project = copy ( self )
      let l:project.xml [ 'Name'    ] = a:1
      let l:project.xml [ 'Date'    ] = a:2
      let l:project.xml [ 'File'    ] = a:3
      let l:project.xml [ 'VCS'     ] = VCS.VCS         ( a:4[1], a:4[2] )
      let l:project.xml [ 'WebHost' ] = WebHost.WebHost ( a:5[1], a:5[2] )
      return l:project
   endif

endfunction

function! Project.GetFromUser ( )

   let l:project = copy ( self )
   let l:project.xml [ 'Name'    ] = input ( 'Please enter project  name.' )
   let l:project.xml [ 'Date'    ] = strftime ( "%d/%m/%y" )
   let l:project.xml [ 'File'    ] = system ( 'find ./ -name "*.proj" )
   let l:project.xml [ 'VCS'     ] = VCS.VCS         ( 'USER' )
   let l:project.xml [ 'WebHost' ] = WebHost.WebHost ( 'USER' )
   return l:project

endfunction
