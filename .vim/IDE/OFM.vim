" ===========================================================================
"  File          :    OFM.vim
"  Entity        :    IDE
"  Purpose       :    Orthodox File Manager
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 27-Jul-2013
" ===========================================================================
let OFM = { }

function! OFM.OFM ( ... )
   if ( a:0 == 0 )
      return copy ( self )
   else
      let l:ofm = copy ( self )
      let l:ofm [ 'Name'     ] = a:1
      let l:ofm [ 'FileTree' ] = a:2
      return l:ofm
   endif
endfunction

function! OFM.Print ( )
   let l:bufferFileName = /tmp/ . self [ 'Name' ] . 'ofm'
   execute 'edit ' . l:bufferFileName
   self . PrintFileTree ( )
endfunction

function! OFM.PrintFileTree ( ... )
   if ( a:0 == 0 )
      let l:fileTree = self [  'FileTree' ]
      let l:fileTreeLevel = 0
   else
      let l:fileTree = a:1
      let l:fileTreeLevel = a:2
   endif

   let l:treeLevelString = ' '
   for l:levelIndex in [ 0 : l:fileTreeLevel ]
      let l:treeLevelString .= '+ '
   endfor

   for l:node in l:fileTree
      if ( type ( l:node ) == type ( '' )
         setline ( line ( '.' ), l:treeLevelString . l:node )
      elseif ( type ( l:node ) == type ( { } )
         setline ( line ( '.' ), keys ( l:node ) [ 0 ] )
         self . PrintFileTree ( l:node , l:fileTreeLevel + 1 )
      endif
      cursor ( line ( '.' ) + 1, 0 )
   endfor
endfunction
