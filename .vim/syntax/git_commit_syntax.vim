"Vim syntax file

"Check if the current syntax has already 
"been loaded.
if exists("b:git_commit_syntax")
   finish
endif

"Define Keywords and matches
""Diff keywords.
syn match   DffMtc    '.*'
syn keyword DffInpKwd in  nextgroup=DffMtc skipwhite
syn keyword DffOutKwd out nextgroup=DffMtc skipwhite

""Previous commit keywords
syn match   PrvComAutMtc '\S\+\s*\S*' 
syn keyword PrvComAutKwd Author nextgroup=PrvComAutMtc skipwhite
"?? Not sure about the following match.
syn match   PrvComDatMtc '\d\d\/\d\d\/\d\d\d\d'
syn keyword PrvComDatKwd Date   nextgroup=PrvComDatMtc skipwhite

""??Note that the Message: keyword has not been applied yet, so we should enter it later.
syn match   PrvComMsgMtc '.*'
syn keyword PrvComMsgKwd Mesage nextgroup=PrvComMsgMtc skipwhite

""Git default commit report keywords
syn match   GitRptModMtc '.*'
"??Not sure about the skipwhite in this line
syn keyword GitRptModKwd modified nextgroup=GitRptModMtc skipwhite

""Define regions
"Previous commit messages region
syn region PrvCom start="Previous commit messages:" end="Modified files that have not been committed:"  contains=PrvComAutKwd,PrvComDatKwd,PrvComMsgKwd

"Enable the loaded syntax flag
let b:git_commit_syntax = "git_commit_syntax"

autocmd BufRead,BufNewFile COMMIT_EDITMSG setfiletype git_commit_syntax


hi def link DffInpKwd DiffText
hi def link DffOutKwd NonText
