" ****** Source code for bash commands ****** "
"Remove command
autocmd BufEnter * :nnoremap rmb :call Bsh('rmb')<CR>

"Function for removing the current buffer
function! Bsh(cmd)
   "Execute the command
   if (a:cmd == 'rmb')
      "Save the current's buffer name
      let l:bufNme = expand('%:p')

      "Remove the buffer.
      execute '!rm ' . l:bufNme

      "Close the buffer so that you don't take warnings on deletion.
      execute ':q!'
   endif
endfunction

"Chmod command
autocmd BufEnter * :nnoremap chmpx :execute '!chmod +x ' . expand('%:p')<CR>
autocmd BufEnter * :nnoremap chmmx :execute '!chmod -x ' . expand('%:p')<CR>
autocmd BufEnter * :nnoremap chmpw :execute '!chmod +w ' . expand('%:p')<CR>
autocmd BufEnter * :nnoremap chmmw :execute '!chmod -w ' . expand('%:p')<CR>
autocmd BufEnter * :nnoremap chmpr :execute '!chmod +r ' . expand('%:p')<CR>
autocmd BufEnter * :nnoremap chmmr :execute '!chmod -r ' . expand('%:p')<CR>

"Execute command
autocmd BufEnter,BufNew *.sh :nnoremap exe :execute '!bash ' . expand('%:p')<CR>

"Execute man command on the command under the cursor.
autocmd BufEnter,BufNew *.sh :nnoremap man :execute '!man ' expand('<cword>')<CR>
