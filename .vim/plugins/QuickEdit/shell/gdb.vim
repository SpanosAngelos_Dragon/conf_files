" ****** A source code for gdb ****** "
"gdb for Intel processors
autocmd BufEnter,BufNew *.c :nnoremap gdbi  <ESC>:call GDB('gdb')<CR>
autocmd BufEnter,BufNew *.c :nnoremap gdbti <ESC>:call GDB('gdbtui')<CR>
"gdb for AVR processors
autocmd BufEnter,BufNew *.c :nnoremap gdba  <ESC>:call GDB('avr-gdb')<CR>
autocmd BufEnter,BufNew *.c :nnoremap gdbta <ESC>:call GDB('avr-gdbtui')<CR>

function! GDB(bin)
	let l:bseFleNme = expand('%:h') . '/' . expand('%:h:t')
   :execute '!' . a:bin . ' ' . l:bseFleNme . '.elf' '-command ' . l:bseFleNme . '.gdb'
endfunction
