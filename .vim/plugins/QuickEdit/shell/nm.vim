autocmd BufEnter,BufNew ?akefile,*.c :nnoremap nmob :call NM('o')<CR>
autocmd BufEnter,BufNew ?akefile,*.c :nnoremap nmeb :call NM('elf')<CR>

"Function for executing the nm program on the buffer's product
function! NM(ext)
   execute '!nm ' . expand('%:h') . '/' . expand('%:h:t') . '.' . a:ext . '| less'
endfunction
