" ****** Set of functions for shell ****** "

"Source Version Control Functions
source ~/.vim/plugins/QuickEdit/shell/git/main.vim

"Compilation shortcuts
source ~/.vim/plugins/QuickEdit/shell/make/main.vim

"gdb shortuts
source ~/.vim/plugins/QuickEdit/shell/gdb.vim

"nm shortcuts
source ~/.vim/plugins/QuickEdit/shell/nm.vim

"Bash shortcuts
source ~/.vim/plugins/QuickEdit/shell/bash.vim
