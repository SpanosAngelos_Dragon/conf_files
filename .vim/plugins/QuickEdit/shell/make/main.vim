" ****** Compilation Shortcuts ***** "
nnoremap mkep :execute '!make'<CR>
nnoremap malp :execute '!make all'<CR>
nnoremap mclp :execute '!make clean'<CR>
nnoremap minp :execute '!make install'<CR>
nnoremap mkc  :execute '!make '  . expand('<cword>')<CR>
nnoremap minb :call Make('install', '')<CR>
nnoremap mkob :execute Make('', expand('%:h:t') . '.o' )<CR>
nnoremap mkxb :execute Make('', expand('%:h:t') . '.hex')<CR>
nnoremap mkeb :execute Make('', expand('%:h:t') . '.elf')<CR>
nnoremap mclb :execute Make('clean', '')<CR>
nnoremap opnm :execute 'e Makefile'<CR>
nnoremap mkgb :execute Make('gdb', '')<CR>
nnoremap mgtb :execute Make('gdbtui', '')<CR>

function! Make(fun, trg)
   "Check the function
   execute '!make -C ' . expand('%:h') . ' ' . a:trg . ' ' . a:fun
endfunction
