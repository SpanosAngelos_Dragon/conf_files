" ******: Source for controlling GIT within the vim editor ****** "
nnoremap gcom :call Git('commit')<CR>
nnoremap gadd :call Git('add')<CR>
nnoremap gsta :call Git('status')<CR>
nnoremap glog :call Git('log')<CR>
nnoremap grm  :call Git('rm')<CR>
nnoremap gch  :call Git('checkout')<CR>

"Switch for C code editing
let s:c = 0
autocmd BufEnter *.c,*.h :let s:c = 1
autocmd BufLeave *.c,*.h :let s:c = 0

"Implementation of the git commands.
function! Git(fun)
   execute 'sp'
   "Save the current working directory.
   let l:cwd = expand(getcwd())
   
   "Go locally to the directory of the buffer
   lcd %:p:h

   "If the command is commit, print previous commit messages.
   "They are picked up from the prepare-commit-msg script in the .git/hooks folder.
   if (a:fun == 'commit')
      execute '!git log ' . expand('%:t') . ' > /home/dragon/.git/COMMIT_EDITMSG.git.log' 
   endif
   
   "Execute the git function
   execute '!git ' . a:fun . ' ' . expand('%:t')

   "Close the buffer
   execute ':q'

   "Check if the edited code is C.
   if (s:c == 1) 
      "Update the tags file
      execute TagCrt()
   endif
endfunction
