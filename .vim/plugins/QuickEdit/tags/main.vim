" ****** Source code for tag files creation ****** "
autocmd BufEnter,BufNew *.c,*.h :nnoremap tagc :execute TagCrt()<CR>
autocmd BufEnter,BufNew *.c,*.h :nnoremap tago :execute TagOpn()<CR>

"Tag file creation
function! TagCrt()
   "Create the tags file.
   if !empty(glob("./.ctags.sh"))
      " Execute the custom ctags generation script, if it exists.
      execute '!./.ctags.sh'
   else
      " Execute the generic ctags generation script.
      execute '!ctags -R ./*'
   endif
endfunction

"Tag open file
function! TagOpn()
   "Check if the tag file exists
   if empty(glob('tags'))
      execute TagCrt()
   endif

   "Open the tag under the cursor
   execute ':0tag'
endfunction
