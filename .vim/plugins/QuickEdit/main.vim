" ***** QuickEdit Plugin ***** "
" Plugin that handles the following operations
"     Functionality
"  Adding comments 
"  Opening files
"  Tracing ports (VHDL Only)
"  Abbreviations (VHDL Only)

"Source VIM script for opening and creating files related to the text under the cursor
source ~/.vim/plugins/QuickEdit/open/main.vim

"Source VIM script for commenting any source code with // combination
source ~/.vim/plugins/QuickEdit/comment/main.vim

"Source VIM script containing abbreviations used in Insert mode
source ~/.vim/plugins/QuickEdit/abbr/main.vim

"Source VIM script for copying components
source ~/.vim/plugins/QuickEdit/copy/main.vim

"Source VIM Shell script
source ~/.vim/plugins/QuickEdit/shell/main.vim

"Source Vim Script for tagging project
source ~/.vim/plugins/QuickEdit/tags/main.vim

"Source Vim Script source script
source ~/.vim/plugins/QuickEdit/source.vim

"Source command for getting help.
source ~/.vim/plugins/QuickEdit/hlp/main.vim
