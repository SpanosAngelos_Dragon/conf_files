function! TglSrcHed(win)

   " Check the current extension type.
   if (expand('%:e') == 'h')
      " The file to be opened is the source code.
      if !empty(glob(expand('%:r') . '.c'))
         let l:ext = 'c'
      elseif !empty(glob(expand('%:r') . '.cpp'))
         let l:ext = 'cpp'
      else 
         return
      endif
   else
      " The file to be opened is the header.
      let l:ext = 'h'
   endif
   
   "Check if the file should be opened in the window 
   "of the current buffer or in a new buffer's window.
   if (a:win == 'new') 
      execute 'sp ' . expand('%:r') . '.' . l:ext
   else
      execute 'e '  . expand('%:r') . '.' . l:ext
   endif
endfunction

autocmd BufEnter,BufNew *.c,*.h,*.cpp nnoremap togn :execute TglSrcHed('new')<CR>
autocmd BufEnter,BufNew *.c,*.h,*.cpp nnoremap togc :execute TglSrcHed('cur')<CR>
