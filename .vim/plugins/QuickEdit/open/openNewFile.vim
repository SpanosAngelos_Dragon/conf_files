let s:debug = 0

"Plugin for inserting VHDL Header file
autocmd BufNewFile *.vhd          :call OpenNewEntity()<CR>
autocmd BufNewFile *akefile*      :call OpenNewMakefile()<CR>
autocmd BufNewFile *.sh           :call OpenNewBashScript()<CR>
autocmd BufNewFile *.c,*.h,*.cpp  :call OpenNewCFile()<CR>
autocmd BufNewFile *.v,*.sv,*.svh :call OpenNewVer()<CR>
autocmd BufNewFile *.vim          :call OpenNewVimScr()<CR>
autocmd BufNewFile *.py           :call OpenNewPytScr()<CR>

"VHDL extensions
let s:vhdPckExt = '_pack.vhd'
let s:vhdCfgExt = '_cfg.vhd'
let s:vhdTbExt  = '_tb.vhd'

"Makefile extenstions
let s:mkfRulExt = '.rules'
let s:mkfRotExt = 'Makefile'
let s:mkfModExt = 'makefile'

"Bash Script extension
let s:bshScrExt = '.sh'

"C extensions
let s:cSrcExt = '.c'
let s:cHedExt = '.h'

"CPP extension
let s:cppSrcExt = '.cpp'

"Verilog extensions
let s:vrlModExt = '.v'

"Quick Open file initial file name
let s:qopFleIni = '.open'

"Vim-scripts filename extension
let s:vimExt = '.vim'

"Python-scripts filename extension
let s:pyExt = '.py'

"Prototype files folder
let s:prtFld    = '~/.vim/plugins/QuickEdit/open/prt/'
let s:vhdPrtFld = s:prtFld . 'vhd/'
let s:mkfPrtFld = s:prtFld . 'mkf/'
let s:bshPrtFld = s:prtFld . 'bsh/'
let   s:cPrtFld = s:prtFld . 'c/'
let s:vrlPrtFld = s:prtFld . 'vrl/'
let s:qopPrtFld = s:prtFld . 'qop/'
let s:vimPrtFld = s:prtFld . 'vim/'
let s:pytPrtFld = s:prtFld . 'pyt/'

"Prototype files
let s:hdrPrt    = s:prtFld    . 'header.txt'
let s:cppHdrPrt = s:prtFld    . 'header.cpp'
let s:vhdPckPrt = s:vhdPrtFld . 'pck.vhd'
let s:vhdCfgPrt = s:vhdPrtFld . 'cfg.vhd'
let s:vhdEntPrt = s:vhdPrtFld . 'ent.vhd'
let s:vhdTbPrt  = s:vhdPrtFld . 'tb.vhd'
let s:mkfRulPrt = s:mkfPrtFld . 'Makefile' . s:mkfRulExt
let s:mkfRotPrt = s:mkfPrtFld . s:mkfRotExt
let s:mkfModPrt = s:mkfPrtFld . s:mkfModExt
let s:vrlModPrt = s:vrlPrtFld . s:vrlModExt
let s:qopModPrt = s:bshPrtFld . s:qopFleIni . s:bshScrExt
let s:vimModPrt = s:vimPrtFld . 'scr.vim'
let s:pytModPrt = s:pytPrtFld . 'scr.pyt'

"Function that upon of the open of a non-existent vim-script writes a basic skeleton.
function! OpenNewPytScr()
   let l:bufNme = expand('%:t')

   " Copy the header
   call WriteHeader('pyt')
  
   "Format the new file
   let l:pytNme = expand('%:p:h:t')
   call OpenNewFileFormatPrototype(l:pytNme, l:bufNme)

   "Save the file
   call write
endfunction


"Function that upon of the open of a non-existent vim-script writes a basic skeleton.
function! OpenNewVimScr()
   let l:bufNme = expand('%:t')

   " Copy the header
   call WriteHeader('vim')
  
   "Format the new file
   let l:vimNme = expand('%:p:h:t')
   call OpenNewFileFormatPrototype(l:vimNme, l:bufNme)

   "Save the file
   call write
endfunction

"Function that upon of the open of a non-existent C-File writes a basic skeleton.
function! OpenNewVer()
   let l:bufNme = expand('%:t')

   " Copy the header
   call WriteHeader('vrl')

   " Copy the protype file.
   execute 'r !cat ' . s:vrlModPrt
   
   "Format the new file
   let l:vrlFleNme = expand('%:p:t:r')
   call OpenNewFileFormatPrototype(l:vrlFleNme, l:bufNme)
endfunction

"Function that upon of the open of a non-existent C-File writes a basic skeleton.
function! OpenNewCFile()
   let l:bufNme = expand('%:t')

   " Copy the header
   execute 'read !cat ' . s:cppHdrPrt
   
   "Format the new file
   let l:cFleNme = expand('%:p:h:t')
   call OpenNewFileFormatPrototype(l:cFleNme, l:bufNme)
endfunction

"Function that upon of the open of a non-existent Makefile writes a basic skeleton.
function! OpenNewBashScript()
   let l:bufNme = expand('%:t')

   " Write the interpreter
   execute 'r !printf "\#\! `which bash`"'

   " Copy the header
   call WriteHeader('sh')
  
   " Copy .open.sh file contents.
   if (strpart(l:bufNme, 0, strlen(s:qopFleIni)) == s:qopFleIni)
      " Copy the body of the script.
      execute 'r !cat ' . s:qopModPrt

      " Write the program name.
      let l:prgNme = tolower(strpart(l:bufNme, strlen(s:qopFleIni), strlen(l:bufNme) - strlen(s:bshScrExt) - strlen(s:qopFleIni)))
      execute '%s/<program>/' . l:prgNme . '/g'
   end if;
   "Format the new file
   let l:bshNme = expand('%:p:h:t')
   call OpenNewFileFormatPrototype(l:bshNme, l:bufNme)

   "Save the file
   call write
endfunction

"Function that upon of the open of a non-existent Makefile writes a basic skeleton
function! OpenNewMakefile()
   let l:bufNme = expand('%:t')

   " Copy the header
   call WriteHeader('makefile')
   
   "Copy the standard source code to the existing source code.
   "Check if the file is a rules file
   if (strpart(l:bufNme, strlen(l:bufNme) - strlen(s:mkfRulExt), strlen(l:bufNme)) == s:mkfRulExt)
      execute 'read !cat ' . s:mkfRulPrt
   "Check if the new flie is a root Makefile for the project
   elseif (l:bufNme == s:mkfRotExt)
      execute 'read !cat ' . s:mkfRotPrt
   "Check if the new file is a module makefile
   elseif (l:bufNme == s:mkfModExt)
      execute 'read !cat ' . s:mkfModPrt
   endif
   
   "Format the new file
   let l:mkfNme = expand('%:p:h:t')
   call OpenNewFileFormatPrototype(l:mkfNme, l:bufNme)
endfunction

"Function that upon of the open of a non-existent VHDL file writes a basic skeleton
function! OpenNewEntity()
   let l:bufNme = expand('%:t')

   "Copy the header
   call WriteHeader('vhdl')
   
   "Copy the standard source code to the existing source code.
   "Check if the file is a package
   if (strpart(l:bufNme, strlen(l:bufNme) - strlen(s:vhdPckExt), strlen(l:bufNme)) == s:vhdPckExt)
      execute 'read !cat ' . s:vhdPckPrt
   "Check if the new flie is a configuration
   elseif (strpart(l:bufNme, strlen(l:bufNme) - strlen(s:vhdCfgExt), strlen(l:bufNme)) == s:vhdCfgExt)
      " Not supported yet -- prototype file is missing
      execute 'read !cat ' . s:vhdCfgPrt
   "Check if the new file is a testbench
   elseif (strpart(l:bufNme, strlen(l:bufNme) - strlen(s:vhdTbExt), strlen(l:bufNme)) == s:vhdTbExt)
      execute 'read !cat ' . s:vhdTbPrt
   "The new file is an entity
   else
      execute 'read !cat ' . s:vhdEntPrt
   endif
   
   " Format the new file
   let l:entNme = strpart(l:bufNme, 0, strlen(l:bufNme) - strlen('.vhd'))
   call OpenNewFileFormatPrototype(l:entNme, l:bufNme)
endfunction

"A function for formatting the output of OpenNew... Functions
function! OpenNewFileFormatPrototype(entNme, fleNme)
   "Delete the empty line that the r!<command> has created.
   normal ggdd

   "Replace <file> with the name of the buffer
   execute '%s/<file>/' . a:fleNme . '/g'

   "Replace <name> with the name of the buffer
   execute '%s/<name>/' . a:entNme . '/g'

   "Replace the <date> with the current date
   let l:date = strftime('%d-%b-%Y')
   execute '%s/<date>/' . l:date . '/g'

   "Move the cursor at the start of the buffer
   normal gg
endfunction

"A function that creates the header
function! WriteHeader(fleTpe)
   "Comment Token. We set as a default the hash since it is quick common among the scripting languages
   if (a:fleTpe == 'vhdl') 
      let l:comTok = '--' 
   elseif (a:fleTpe == 'vim')  
      let l:comTok = '"'
   elseif (or (a:fleTpe == 'c', a:fleTpe == 'vrl'))
      let l:comTok = '\/\/'
   else
      let l:comTok = '#'
   endif
   if (s:debug == 1)
      echo l:comTok
   endif

   "Copy the header
   execute 'read !cat ' . s:hdrPrt
   "Set the proper comment tokens defined at ../comment/main.vim
   execute '%s/--/' . l:comTok .  '/g'
endfunction
