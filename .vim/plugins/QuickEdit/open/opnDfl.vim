"Default function for opening a file
function! OpnDfl(fle, fld, win)
   " Check the destination buffer
   if (a:win == 'cur')
      let l:opnCom = 'edit '
   else
      let l:opnCom = 'split '
   endif

   " Check if the current buffer is a latex document and if so
   " append the .tex expansion at the end of the search.
   let s:fle = a:fle
   if (expand('%:e') == 'tex')
      let s:fle .= '.tex'
   endif

   " Check if the relative or absolute path leads to an existing file.
   if !empty(glob(s:fle))
      execute l:opnCom . s:fle
   elseif !empty(glob(expand('%:h') . '/' . s:fle))
      execute l:opnCom . expand('%:h') . '/' . s:fle
   "Open a new file.
   else
      "Check if the path given is an absolute path.
      if (strpart(s:fle, 0, 1) == '~')
         execute '!mkdir -p ' . a:fld
         execute l:opnCom . s:fle
      elseif (strpart(s:fle,0, 1) == '/')
         execute '!mkdir -p ' . a:fld
         execute l:opnCom . s:fle
      else
         execute '!mkdir -p ' . a:fld
         execute l:opnCom . expand('%:h') . '/' . s:fle
      endif
   endif
endfunction
