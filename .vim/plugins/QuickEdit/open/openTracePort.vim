"OpenEntityDeclaration This functions does the following
" 1. Opens the file of the entity that holds the port that is connected with the signal under the cursor
" 2. Opens searches for the place where the port gets it's value in the new entity file just opened.
function! OpenTracePort(portUnderCursor)
   "Go to the entity name associated with the port under the cursor
   execute '/ port ' 
   normal  N
   normal  b

   " Open the declaration file of the entity
   execute QuickOpenVHDL(expand('<cword>'))

   "Search for the port
   execute '/' . a:portUnderCursor
   
endfunction
   
