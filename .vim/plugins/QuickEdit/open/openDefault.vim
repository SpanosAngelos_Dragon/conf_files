"Default function for opening a file
function! OpenDefault(fle)
   " Check if the relative or absolute path leads to an existing file.
   if !empty(glob(a:fle))
      execute 'edit ' . a:fle
   elseif !empty(glob(expand('%:h') . '/' . a:fle))
      execute 'edit ' . expand('%:h') . '/' . a:fle
   "Open a new file.
   else
      "Check if the path given is an absolute path.
      if (strpart(a:fle, 0, 1) == '~')
         execute 'edit ' . a:fle
      elseif (strpart(a:fle,0, 1) == '/')
         execute 'edit ' . a:fle
      else
         execute 'edit ' . expand('%:h') . '/' . a:fle
      endif
   endif
endfunction
