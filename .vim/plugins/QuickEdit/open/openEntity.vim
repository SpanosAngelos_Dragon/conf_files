"Map the gf command according to the language used

"Variable to hold all the VHDL tags
let g:vhdTag = {}

"VHDL tag file-name
let s:tagFile='vhdTags'

"Open function for VHDL programs
function! OpenEntity(entityUnderCursor)
   "Get the filname with the definition of the entity
   let l:file2Open = GetEntDefFleNme(a:entityUnderCursor)
   
   "Open the entity definition file
   if (strlen(l:file2Open) == 0)
      echo "File was not found in the tags list"
   elseif empty(glob(l:file2Open))
      echo "File has been moved from its tagged place"
      echo "Please run the vhdTags script from here"
   else
      :execute "edit " . l:file2Open
   endif
   return
endfunction


function! GetEntDefFleNme(entityUnderCursor)
   let g:vhdTag = {}
   "Check if the VHDL tag files have been parsed.
   if (g:vhdTag == {})
      "Check if the VHDL tag file exists
      if (empty(glob(s:tagFile)))
         :execute !vhdTags >> s:tagFile
      endif
      "Put all the contents of the tagFile into a list
      let l:curBuf = bufnr(bufname('%'))
      execute 'silent edit ' . s:tagFile
      let l:tagFileList = getbufline(bufnr(bufname(s:tagFile)),1, "$")
      execute 'buffer' . curBuf
      execute 'bwipeout ' . s:tagFile

      "Pass the contents of the list to a dictionary
      for l:curLine in l:tagFileList
         let l:entity = substitute(curLine, ".*\.vhd:", "", "")
         let l:entityFile = substitute(curLine, ":.*", "", "")
         let g:vhdTag[l:entity] = l:entityFile
      endfor
      "Flag on parsed VHDL files
      let s:tagFilesParsedFlag = 1
   endif

   "Open the file that corresponds to the text under the cursor
   echo a:entityUnderCursor
   return g:vhdTag[a:entityUnderCursor]
endfunction
