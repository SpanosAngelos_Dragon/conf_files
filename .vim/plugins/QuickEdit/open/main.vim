" ***** Source code for open ***** "
"     HotKey  FileType Function
"  1.  (gf)   C        Opens the file under the cursor 
"  2.  (gf)   VHDL     Opens the file where the entity under the cursor has been declared
"  3.  (tr)   VHDL     Opens the file where the entity of the port under the cursor

" Source the associated functions
source ~/.vim/plugins/QuickEdit/open/openEntity.vim
source ~/.vim/plugins/QuickEdit/open/openNewFile.vim
source ~/.vim/plugins/QuickEdit/open/openTracePort.vim
source ~/.vim/plugins/QuickEdit/open/togHedSrc.vim
source ~/.vim/plugins/QuickEdit/open/opnDfl.vim

" Map the gf key to the open function according to the file-type of the current buffer
autocmd BufEnter * :nnoremap gfc :call OpnDfl(expand('<cfile>'), expand('<cfile>:p:h'), 'cur')<CR>
autocmd BufEnter * :nnoremap gfn :call OpnDfl(expand('<cfile>'), expand('<cfile>:p:h'), 'new')<CR>
autocmd BufEnter *.vhd :nnoremap gf  :call OpenEntity(expand('<cfile>:t'))<CR>
autocmd BufEnter *.vhd :nnoremap tr ^:call OpenTracePort(expand('<cword>'))<CR>



