" ****** Source code for sourcing files ****** "

"Source commands for the .Xmodmap file.
autocmd BufEnter,BufNewFile *.Xmodmap :nnoremap src :execute '!xmodmap ' . expand('%:p')<CR>
autocmd BufLeave .Xmodmap :nunmap src

"Source commands for the .xbindkeysrc file.
autocmd BufEnter,BufNewFile *.xbindkeysrc :nnoremap src :execute '!xbindkeys -f ' . expand('%:p')<CR>
autocmd BufLeave *.xbindkeysrc :nunmap src
