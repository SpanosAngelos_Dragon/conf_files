" ***** Abbreviations ***** "
" Variable declaration helper abbreviations
autocmd BufEnter *.v :iab prm parameter
autocmd BufEnter *.v :iab inp input
autocmd BufEnter *.v :iab otp output
autocmd BufEnter *.v :iab `i  `include "
autocmd BufEnter *.v :iab emd endmodule

autocmd BufEnter *.v :iab prr always @ (negedge resetn or posedge clk)
\<CR>   if (!resetn)
\<CR>
\<CR>else
\<CR>
\<Up>>        <Left><Right>

autocmd BufEnter *.v :iab pr always @ (posedge clk)
\<CR>   
