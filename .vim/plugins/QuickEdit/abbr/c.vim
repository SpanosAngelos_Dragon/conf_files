" ***** Abbreviations ***** "
" Port I/O
autocmd BufEnter *.c,*.h :iab #i #include
autocmd BufEnter *.c,*.h :iab #d #define
