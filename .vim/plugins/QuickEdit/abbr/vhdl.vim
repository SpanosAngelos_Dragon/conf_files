" ***** Abbreviations ***** "
" Port I/O
autocmd BufEnter *.vhd :iab clk clk    : in  std_logic;
\<CR>resetn : in  std_logic;
autocmd BufEnter *.vhd :iab il in  std_logic
autocmd BufEnter *.vhd :iab ol out std_logic
autocmd BufEnter *.vhd :iab iv in  std_logic_vector
autocmd BufEnter *.vhd :iab ov out std_logic_vector

" Process
" Non - Resettable process
autocmd BufEnter *.vhd :iab pr  process(clk)
\<CR>begin
\<CR>   if (clk'event and clk = '1') then
\<CR>
\<CR>end if;
\<CR><Left><Left><Left>end process;<Up><Up>        

" Resettable process.  Left-Right placed at the end so that the space is not lost.
autocmd BufEnter *.vhd :iab prr  process(clk, resetn)
\<CR>begin
\<CR>   if (resetn = '0') then
\<CR>
\<CR>elsif (clk'event and clk = '1') then
\<CR>
\<CR>end if;
\<CR><Left><Left><Left>end process;<Up><Up>         
\<Up><Up>        <Left><Right>

" Variable declaration helper abbreviations
autocmd BufEnter *.vhd :iab sl std_logic
autocmd BufEnter *.vhd :iab sv std_logic_vector
autocmd BufEnter *.vhd :iab do downto 0);
autocmd BufEnter *.vhd :iab si signal
