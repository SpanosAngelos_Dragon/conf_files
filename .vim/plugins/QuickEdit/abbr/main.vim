"***** Source code for QuickEdit ***** "

" Clean-up the space from insert and command abbreviations every time you leave a buffer
autocmd BufLeave * :iabclear
autocmd BufLeave * :cabclear

"Generic Abbreviations
source ~/.vim/plugins/QuickEdit/abbr/gen.vim

"VHDL Abbreviations
source ~/.vim/plugins/QuickEdit/abbr/vhdl.vim

"C Abbreviations
source ~/.vim/plugins/QuickEdit/abbr/c.vim

"Verilog Abbreviations
source ~/.vim/plugins/QuickEdit/abbr/verilog.vim

"Xilinx User Constraints file abbreviations
source ~/.vim/plugins/QuickEdit/abbr/ucf/main.vim
