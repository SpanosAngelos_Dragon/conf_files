"***** Source code for QuickComment ***** "
"  HotKey   FileType    Function
"    //       (any)     Toggles comments blocks

"Comment Token. We set as a default the hash since it is quick common among the scripting languages
let g:quickCommentToken = '#'

"Set the comment token according to the file type of the current buffer
autocmd BufEnter *.vim         :let g:quickCommentToken='"'
autocmd BufEnter .vimrc        :let g:quickCommentToken='"'
autocmd BufEnter *.c,*.cpp,*.h :let g:quickCommentToken='//'
autocmd BufEnter *.h           :let g:quickCommentToken='//'
autocmd BufEnter *.vhd         :let g:quickCommentToken='--'
autocmd BufEnter .Xmodmap      :let g:quickCommentToken='!'
autocmd BufEnter ?akefile      :let g:quickCommentToken='#'
autocmd BufEnter *.v           :let g:quickCommentToken='//'
autocmd BufLeave *             :let g:quickCommentToken='#'


"Map the // key to the QuickCommentToggle function
nnoremap // :call CommentToggleLine(getline('.'))<CR>
vnoremap // :call CommentToggleBlock()<CR>
"iab // :call echo('g:quickCommentToken')

"Source the comment sub-functions
source ~/.vim/plugins/QuickEdit/comment/toggle.vim

"Add the comments character while on edit mode when going to the next line.
set fo+=cro
