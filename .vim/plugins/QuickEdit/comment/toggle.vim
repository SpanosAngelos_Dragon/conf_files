function! GetCommentToggleLine(curLine)
   " Check if the code is commented or not
   if (strpart(a:curLine, 0, strlen(g:quickCommentToken)) == g:quickCommentToken)
      " Remove the commemts
      return strpart(a:curLine, strlen(g:quickCommentToken), strlen(a:curLine))
   else
      " Insert the comments
      return g:quickCommentToken . a:curLine
   endif
endfunction

"Function which toggles the comments of the visual block
function! CommentToggleLine(curLine)
   let l:crsLne = line(".")
   let l:crsCol = virtcol(".")
   :execute setline(line("."), GetCommentToggleLine(a:curLine))
   :call cursor(l:crsLne+1, l:crsCol)
endfunction


"Function for quick commenting of a block
function! CommentToggleBlock() range
   let l:crsLne = line(".")
   let l:crsCol = virtcol(".")
   "Step through each line in the range
   for l:linNum in range(a:firstline, a:lastline)
      :execute setline(l:linNum, GetCommentToggleLine(getline(l:linNum)))
   endfor
   call cursor(a:lastline + 1, l:crsCol)
endfunction
