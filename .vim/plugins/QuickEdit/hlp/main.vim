" ===========================================================================
"  File          :    hlp.vim
"  Entity        :    hlp
"  Purpose       :    To open the help screen under different contexts.
"  Documentation :   
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 14-Aug-2012
" ===========================================================================

nnoremap man :call Help(expand('<cword>'))<CR>

function! Help(obj)
   " Help command implementation for the Vim editor.
   if (expand('%:e') == 'vim')
      execute ':help ' . a:obj
   endif
endfunction
