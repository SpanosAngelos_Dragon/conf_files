let s:debug = 0

"Map the Control + i key combination to create
"a component instantiation of the entity under the cursor

autocmd BufEnter *.vhd :map cc :call CopyVHDLComp(expand('<cword>'))<CR>

function! CopyVHDLComp(entity)
   "Open silently the entity definition file
   let l:curBuf = bufnr(bufname('%'))
   let l:crsCol = virtcol(".")
   let l:crsLne = line(".")
   let l:entFleNme = GetEntDefFleNme(a:entity)
   execute 'edit ' . l:entFleNme
   if (s:debug == 1)
      echo (l:entFleNme)
   endif
   
   "Yank the entity definition body
   normal gg
   execute '/' . a:entity
   normal wwhv
   execute '/\<end\>'
   call cursor(line(".") - 1, virtcol("."))
   execute '/' . a:entity
   execute '/$'
   normal y
   "Close the entity definition file
   execute 'buffer' . l:curBuf
   execute 'bwipeout' . l:entFleNme

   "Save the start line of the component 
   let l:strLne = line(".")

   "Paste the definition file
   normal $
   normal p
   execute '/\<end\>'
   execute '.s/entity/component/'

   "Save the end line of the component
   let l:endLne = line(".")

   "Indent right the component
   for l:curLne in range (l:strLne + 1, l:endLne)
      call cursor(l:curLne, 1)
      execute 's/^/   /g'
   endfor

   "Delete two extra lines that appear at the end of the component
   call cursor(l:endLne + 1,1)
   normal ddDD
endfunction
