" ***** Function for copying a component ***** "
source ~/.vim/plugins/QuickEdit/copy/vhdlComp.vim

" ***** Function for copying a component and doing a basic instantiation ***** "
source ~/.vim/plugins/QuickEdit/copy/vhdlInst.vim
