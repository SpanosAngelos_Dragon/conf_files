" ***** Script for copying and doing a basic instantiation of a component ***** "
autocmd BufEnter *.vhd :nnoremap <silent> ci :call CopyInstantiateVHDL(expand('<cword>'))<CR>

function! CopyInstantiateVHDL(entity)
   "Get the instance name of the module
   normal ^
   let l:insNme = expand('<cword>')
   "Get the start line and column of the component
   let l:strLne = line(".")
   let l:strCol = virtcol(".")

   "Copy the component
   call CopyVHDLComp(a:entity)

   "Get the last line and column of the component
   let l:endLne = line(".")
   let l:endCOl = virtcol(".")

   "Do a basic instantiation
   call cursor(l:strLne)
   execute l:strLne . ',' . l:endLne . 's/is *port/port map/g'
   execute l:strLne . ',' . l:endLne . 's/clk\( *\):.*/clk\1=> clk,/'
   execute l:strLne . ',' . l:endLne . 's/rstn\( *\):.*/rstn\1=> rstn,/'
   execute l:strLne . ',' . l:endLne . 's/: *in.*/=> ,/'
   execute l:strLne . ',' . l:endLne . 's/\(\S\+\)\( *\): *out.*/\1\2=> ' . l:insNme . '\1,/'
   execute l:strLne . ',' . l:endLne . 's/end *component.*//g'
   execute l:endLne - 3              . 's/,//g'
endfunction
