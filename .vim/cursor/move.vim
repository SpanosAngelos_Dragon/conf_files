"Normal mode.
""Shift the move buttons one button to the right so
""that they stand at the natural hand position
nmap ; <Right>
nmap l <Down>
nmap k <Up>
nmap j <Left>

""Map the uppoer move case letters to their lower case
nmap ; <Right>
nmap L <Down>
nmap K <Up>
nmap J <Left>
nmap W w
nmap B b

"Visual mode.
""Shift the move buttons one button to the right so
""that they stand at the natural hand position
vmap ; <Right>
vmap l <Down>
vmap k <Up>
vmap j <Left>

""Map the uppoer move case letters to their lower case
vmap ; <Right>
vmap L <Down>
vmap K <Up>
vmap J <Left> 
vmap W w
vmap B b
