" ===========================================================================
" File    : IDE.vim
"  Entity  : .vim
"  Purpose : Integrated Development Environment
" ===========================================================================
"  Design Engineer : Angelos Spanos (TE)
"  Creation Date   : 15-Jul-2013
" ===========================================================================
source ~/.vim/IDE/Project/VCS.vim
source ~/.vim/IDE/Project/WebHost.vim
source ~/.vim/IDE/Project.vim

source ~/.vim/IDE/RTL.vim

let g:lineIndex = 0
augroup IDEAutoCommands
   autocmd!
   autocmd BufEnter *.proj.vim :let s:vimProject = Project.Project( )
augroup END
