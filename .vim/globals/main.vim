"Container of the global variables

"Auto-commands for source code commets
autocmd BufEnter *.vim  :let g:comTok='"'
autocmd BufEnter *.c    :let g:comTok='/'
autocmd BufEnter *.vhd* :let g:comTok='--'

"Auto-commands for file exntesions
"Used for grouping different extensions
"under a common file-type identifier.
autocmd BufEnter *.vhd  :let g:fileType='vhdl'
autocmd BufEnter *.vhdl :let g:fileType='vhdl'
autocmd BufEnter *.vim  :let g:fileType='vim'
autocmd BufEnter *.c    :let g:fileType='c'
autocmd BufEnter *.h    :let g:fileTYpe='c'
