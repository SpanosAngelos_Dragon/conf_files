"Reduce the save command to two button-taps
nnoremap WW :w<CR>
nnoremap !! :q!<CR>
inoremap <F2> <Esc>:w<CR>i<Right>
