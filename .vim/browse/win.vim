"Commands for increasing/decreasing the buffer window. 
"Ctrl-W= for getting it back to normal
nnoremap -- :vertical resize -1<CR>
nnoremap ++ :vertical resize +1<CR>

"Key combination for maximizing the current buffer.
nnoremap onlb :only<CR>
