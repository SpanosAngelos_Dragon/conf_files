"Source buffer browsing commands
source ~/.vim/browse/buffer.vim

"Source save commands
source ~/.vim/browse/save.vim

"Source exit commands
source ~/.vim/browse/exit.vim

"Souce window configuration
source ~/.vim/browse/win.vim
