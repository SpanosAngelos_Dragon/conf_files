#! /bin/bash
# ===========================================================================
#  File          :    .openEog.sh
#  Entity        :    dragon
#  Purpose       :    To open the Image Viewer (eog) at it's proper location.
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 31-Jul-2012
# ===========================================================================

#Open the evince viewer.
eog &

#Sleep to let the Window Manger put the window to it's list.
sleep 0.5

#Place the viewer at the Left Upper corner covering half the screen.
wmctrl -r 'Image Viewer' -e 1,0,0,958,1020

#Bring to front the image viewer.
wmctrl -a 'Image Viewer'
