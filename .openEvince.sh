#! /bin/bash
# ===========================================================================
#  File          :    .openEvince.sh
#  Entity        :    dragon
#  Purpose       :    
#  Documentation :   
#  Notes         :
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 31-Jul-2012
#  File history    :
# ===========================================================================


#Open the evince viewer.
evince $1 &

#Sleep to let the Window Manger put the window to it's list
sleep 0.5

#Place the viewer at the Left Upper corner covering half the screen.
wmctrl -r 'Document Viewer' -e 1,0,0,958,1020

#Bring to front the document viewer.
wmctrl -a 'Document Viewer'
