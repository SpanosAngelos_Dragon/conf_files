#! /bin/bash
# ===========================================================================
#  File          :    .openWireshrak.sh
#  Entity        :    dragon
#  Purpose       :    
#  Documentation :   
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 14-Aug-2012
# ===========================================================================

#Open the wireshark 
wireshark &

#Sleep to let the Window Manger put the window to it's list
sleep 0.5

#Place the viewer at the Left Upper corner covering half the screen.
wmctrl -r 'The Wireshark Network Analyzer' -e 1,0,0,958,1020

#Bring to front the wireshark.
wmctrl -a 'The Wireshark Network Analyzer'
