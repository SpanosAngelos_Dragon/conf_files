#! /bin/bash
# ===========================================================================
#  File          :    .openSpotify.sh
#  Entity        :    dragon
#  Purpose       :    
#  Documentation :   
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 16-Aug-2012
# ===========================================================================

#Open the spotify.
spotify &

#Sleep to let the Window Manger put the spotify to it's list
sleep 0.5

#Place the spotify t the Left Upper corner covering half the screen.
wmctrl -r 'Spotify Unlimited - Linux Preview' -e 1,0,0,958,1020

#Bring spotify to front.
wmctrl -a 'Spotify Unlimited - Linux Preview'
