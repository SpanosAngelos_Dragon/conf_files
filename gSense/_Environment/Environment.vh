// ===========================================================================
//  File          :    Environment.vh
//  Purpose       :    gSense environment definitions.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 10-Oct-2013
// ===========================================================================
`define INITIAL_CLOCK_HOLD_Z_T 10ns
`define INITIAL_CLOCK_HOLD_0_T 10ns
`define INITIAL_RESET_HOLD_Z_T 10ns
`define INITIAL_RESET_HOLD_0_T 10ns

`define CLOCK_PERIOD 5ns

`define HOST_PRG_EXT    1
`define HOST_PRG_DATA_W 16
`define HOST_PRG_ADDR_W 8

`define SENSOR_PRG_EXT    1
`define SENSOR_PRG_DATA_W 16
`define SENSOR_PRG_ADDR_W 8
