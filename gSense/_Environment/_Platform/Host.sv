//===========================================================================
//  File          :    Host.v
//  Entity        :    Host
//  Purpose       :    
//===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 10-Oct-2013
//===========================================================================
module Host#(
   parameter PRG_DATA_W = 0,
   parameter PRG_ADDR_W = 0,

   localparam PRG_ADDR_W = log2(PRG_DATA_N)
) (
   input  clkP,
   input  aResetNP,
   inout  sdaP,
   output sclP
   `ifdef HOST_PRG_ROM_EXT,
      output [`HOST_ADDR_W -1 : 0 ] prgROMAddr,
      input  [`HOST_DATA_W -1 : 0 ] prgROMData
   `endif
);

`include "_Host/C8051.sv"
C8051 #(
   .RAM_EXT_DATA_W (PRG_DATA_W)
   .RAM_EXT_DATA_N 256,
   .RAM_PRG_DATA_N 1024,
   .ISR_NUM        2,
) c8051 (
   .clkP       (clkP),
   .aresetNP   (aResetNP),
   .sresetNP   (0),
                       
   .isrNP   (0),

   `ifdef HOST_PRG_ROM_EXT,
      .prgROMaddr (prgROMAddr),
      .prgROMdata (prgROMData),
   `endif

   .sbAddrP    (sbAddrW),
   .sbDataWrP  (sbDataWrW),
   .sbDataRdP  (sbDataRdW),
   .sbValidWrP (sbValidWrW),
   .sbValidRdP (sbValidRdW),
   .sbRdNwrP   (sbRdNwrW)
);

`include "_Host/I2CMaster.sv"
I2CMaster #(
   .MODE_STD_SUPPORTED              = `SUPPORTED,
   .MODE_FAST_SUPPORTED             = `SUPPORTED,
   .MODE_FAST_PLUS_SUPPORTED        = `SUPPORTED,
   .MODE_HIGH_SPEED_SUPPORTED       = `SUPPORTED,
   .COLLISION_DETECTION_SUPPORTED   = `NOT_SUPPORTED,
   .CLK_STRETCHING_SUPPORTED        = `NOT_SUPPORTED,
   .TEN_BIT_SLAVE_ADDRESS_SUPPORTED = `NOT_SUPPORTED,
   .GENERAL_CALL_ADDRESS_SUPPORTED  = `NOT_SUPPORTED,
   .SOFTWARE_RESET_SUPPORTED        = `NOT_SUPPORTED,
   .MODE_DEFAULT                    = `MODE_STD,
   .HOST_CLK_PERIOD                 = CLK_PERIOD
) i2cMaster (
   .clkP       (clkP),
   .aResetNP   (aResetNP),
   .sdaP       (sdaP),
   .sclP       (sclP),

   .sbAddrP    (sbAddrW),
   .sbDataWrP  (sbDataWrW),
   .sbDataRdP  (sbDataRdW),
   .sbValidWrP (sbValidWrW),
   .sbValidRdP (sbValidRdW),
   .sbRdNwrP   (sbRdNwrW)

);
endmodule
