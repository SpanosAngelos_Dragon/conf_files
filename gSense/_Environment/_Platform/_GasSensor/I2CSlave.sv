// ===========================================================================
//  File          :    I2CSlave.sv
//  Entity        :    I2CSlave
//  Purpose       :    
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 13-Oct-2013
// ===========================================================================
`include "_I2CSlave/Configuration.vh"
`define ACK_BIT   1'b1
`define START_BIT 1'b1
`define RDNWR_BIT 1'b1
module I2CSlave #(
   parameter MODE_STD_SUPPORTED              = `getSupportValue(MODE_STD),
   parameter MODE_FAST_SUPPORTED             = `getSupportValue(MODE_FAST),
   parameter MODE_FAST_PLUS_SUPPORTED        = `getSupportValue(MODE_FAST_PLUS),
   parameter MODE_HIGH_SPEED_SUPPORTED       = `getSupportValue(MODE_HIGH_SPEED),
   parameter COLLISION_DETECTION_SUPPORTED   = `getSupportValue(COLLISION_DETECTION),
   parameter CLK_STRETCH_SUPPORTED           = `getSupportValue(CLK_STRETCH),
   parameter TEN_BIT_SLAVE_ADDRESS_SUPPORTED = `getSupportValue(TEN_BIT_SLAVE_ADDRESS),
   parameter GENERAL_CALL_ADDRESS_SUPPORTED  = `getSupportValue(GENERAL_CALL_ADDRESS),
   parameter SOFTWARE_RESET_SUPPORTED        = `getSupportValue(SOFTWARE_RESET),
   parameter MODE_DEFAULT                    = `MODE_DEFAULT,
   parameter HOST_CLK_PERIOD                 = 0
   localparam CLK_DIV_COUNT_W                 = `CLK_DIV_COUNT_W
   localparam ADDR_W = `getWidth(),
   localparam MODE_W = `MODE_W,
   localparam DATA_W = `DATA_W
) (
   input  clkP,
   input  aResetNP,
   inout  sdaP,
   output sclP,

   input  [SB_ADDR_W -1 : 0] sbAddrP,
   input  [SB_DATA_W -1 : 0] sbDataWrP,
   output [SB_DATA_W -1 : 0] sbDataRdP,
   input  sbValidWrP,
   output sbValidRdP,
   input  sbRdNwrP
);


`include "_I2CSlave/RegisterBank.svh"

endmodule
