// ===========================================================================
//  File          :    StandardConfiguration.vh
//  Entity        :    StandardConfiguration
//  Purpose       :    I2C Master Standard Configuration
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 12-Oct-2013
// ===========================================================================
`define DATA_W 8

`define MODE_STD_KBPS         100
`define MODE_FAST_KBPS        400
`define MODE_FAST_PLUS_KBPS  1400
`define MODE_HIGH_SPEED_KBPS 3400
`define getSCLDiv(mode, hostClockPeriod) (`mode_KBPS / hostClockPeriod)

`define NOT_SUPPORED 0
`define SUPPORTED    (`NOT_SUPPORED + 1)

`define getSupportValue (x) \
   `ifdef x_SUPPORTED \
      `SUPPORTED \
   `else \
      `NOT_SUPPORED \
   `endif
