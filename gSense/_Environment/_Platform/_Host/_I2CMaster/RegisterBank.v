// ===========================================================================
//  File          :    RegisterBank.v
//  Entity        :    RegisterBank
//  Purpose       :    I2C Register bank.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 12-Oct-2013
// ===========================================================================
`include "_RegisterBank/map.vh"
module RegisterBank #(
   parameter MODE_STD_SUPPORTED         = `getSupportValue(MODE_STD),
   parameter MODE_FAST_SUPPORTED        = `getSupportValue(MODE_FAST),
   parameter MODE_FAST_PLUS_SUPPORTED   = `getSupportValue(MODE_FAST_PLUS),
   parameter MODE_HIGH_SPEED_SUPPORTED  = `getSupportValue(MODE_HIGH_SPEED),
   parameter CLK_STRETCH_SUPPORTED   = `getSupportValue(CLK_STRETCH),

   parameter CONF_MODE_F_W           = `CONF_MODE_F_W,
   parameter CONF_MODE_MSB           = `CONF_MODE_MSB,
   parameter CONF_CLK_STRETCH_F_W = `CONF_CLK_STRETCH_F_W,
   parameter CONF_CLK_STRETCH_MSB = `CONF_CLK_STRETCH_MSB,

   parameter TX_DATA_F_W = `TX_DATA_F_W,
   parameter TX_DATA_MSB = `TX_DATA_MSB,
                                       
   parameter TX_DIR_F_W = `TX_DIR_F_W,
   parameter TX_DIR_MSB = `TX_DIR_MSB,
                                       
   parameter TX_DATA_F_W = `TX_DATA_F_W,
   parameter TX_DATA_MSB = `TX_DATA_MSB,
                                       
   parameter RX_DATA_F_W = `RX_DATA_F_W,
   parameter RX_DATA_MSB = `RX_DATA_MSB,
                                       
   parameter TX_VALID_F_W  = `TX_VALID_F_W,
   parameter TX_VALID_MSB  = `TX_VALID_MSB,
   parameter TX_ENABLE_F_W = `TX_ENABLE_F_W,
   parameter TX_ENABLE_MSB = `TX_ENABLE_MSB,

   parameter ERROR_CODE = `ERROR_CODE,

   parameter HIERARCHICAL_PATH = "",
   parameter NAME              = "RegisterBank",

   localparam FULL_NAME = {HIERARCHICAL_PATH. "/", NAME};
) (
   input  clkP,
   input  aResetNP,

   input  [SB_ADDR_W -1 : 0] sbAddrP,
   input  [SB_DATA_W -1 : 0] sbDataWrP,
   output [SB_DATA_W -1 : 0] sbDataRdP,
   input  sbValidInP,
   output sbValidOutP,
   input  sbRdNwrP,
   `ifdef CLK_STRETCH_SUPPORTED
      output clkStrechingP,
   `endif
   output [CONF_MODE_F_W -1 : 0] modeP,

   output                 opRdNWrP,
   output [DATA_W -1 : 0] txDataP,
   output [ADDR_W -1 : 0] txAddrP,
   output                 txValidP,
   input                  txEnableP
   input  [DATA_W -1 : 0] rxDataP,
   input                  rxValidP,
   output                 rxEnableP,
);

   reg [MODE_W -1 : 0] modeR;
   reg                 clkStretchingR;

   always @ (posedge clkP or negedge aResetNP) : wrB begin
      if (!aResetNP) begin
         modeR          = 0;
         clkStretchingR = 0;
      end
      else if (sbValidInP && !sbRdNwrP) begin
         case (sbAddrP) begin
            CONF_R : begin
               mode = sbDataWrP[CONF_MODE_MSB -1 -: CONF_MODE_F_W];
               `ifdef CLK_STRETCH_SUPPORTED
                  clkStretchingR = sbDataWrP[CONF_CLK_STRETCH_MSB -1 -:CONF_CLK_STRETCH_F_W];
               `endif
            end
            TX_DATA_R : begin
               txAddrR = sbDataWrP[TX_DATA_MSB -1 -: TX_DATA_F_W];
            end
            TX_DIR : begin
               txDirR = sbDataWrP[TX_DIR_MSB -1 -: TX_DIR_F_W];
            end
            TX_DATA : begin
               txDataR = sbDataWrP[TX_DATA_MSB -1 -: TX_DATA_F_W];
            end
            TX_VALID_ENABLE : begin
               txValidR = sbDataWrP[TX_VALID_MSB -1 -: TX_VALID_F_W];
            end
            default : begin
              `ifndef SYNTH
                 $display( "WRN::%s::wrB::Unknown address(%d)", FULL_NAME, sbAddrP);
              `endif
            end
         end
      end
   end
   assign clkStretchingP = clkStretchingR;

   reg [SB_DATA_W -1 : 0] sbDataRdR;
   always @ (posedge clkP) : rdB begin
      if (sbValidInP && sbRdNwrP) begin
         sbValidOutR = 1;
         case sbAddrP begin
            CONF_R : begin
               sbDataRdR[CONF_MODE_MSB -1 -: CONF_MODE_F_W] = modeR;
               sbDataRdR[CONF_CLK_STRETCH_MSB -1 -: CONF_CLK_STRETCH_F_W] = clkStretchingR;
            end
            `ifdef CLK_STRETCH_SUPPORTED
               CLK_STRETCH : begin
                  sbDataRdR[CONF_CLK_STRETCH_MSB -1 -: CONF_CLK_STRETCH_F_W] = clkStretchingR;
               end
            `endif
            TX_DATA : begin
               sbDataRdR[TX_DATA_MSB -1 -: TX_DATA_F_W] = txAddrR;
            end
            TX_DIR : begin
               sbDataRdR[TX_DIR_MSB -1 -: TX_DIR_F_W] = txDirR;
            end
            TX_DATA : begin
               sbDatardR[TX_DATA_MSB -1 -: TX_DATA_F_W] = txDataR;
            end
            TX_VALID_ENABLE : begin
               sbDataRdR[TX_VALID_MSB -1 -: TX_VALID_F_W] = txValidR;
               sbDataRdR[TX_ENABLE_MSB -1 -: TX_ENABLE_F_W] = txEnableP;
            end
            RX_DATA : begin
               sbDatardR[RX_DATA_MSB -1 -: RX_DATA_F_W] = txDataP;
            end
            default : begin
               sbDataRdR = ERROR_CODE;
              `ifndef SYNTH
                 $display( "WRN::%s::wrB::Unknown address(%d)", FULL_NAME, sbAddrP);
              `endif
            end
         endcase
      end
      else begin
         sbValidOutR = 0;
      end
   end
   assign sbDataRdP = sbDataRdR;
endmodule
