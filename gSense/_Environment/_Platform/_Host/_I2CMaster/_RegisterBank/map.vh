// ===========================================================================
//  File          :    map.vh
//  Entity        :    map
//  Purpose       :    I2C Master register bank.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 13-Oct-2013
// ===========================================================================
`define CONF_R            0
`define TX_ADDRESS_R      (`CONF_R       + 1)
`define TX_DIR_R          (`TX_ADDRESS_R + 1)
`define TX_WR_DATA_R      (`TX_DIR_R     + 1)
`define TX_VALID_ENABLE_R (`TX_WR_DATA_R + 1)

`define CONF_MODE_F_W        `MODE_W
`define CONF_MODE_MSB        `CONF_MODE_F_W
`define CONF_CLK_STRETCH_F_W 1
`define CONF_CLK_STRETCH_MSB (`CONF_MODE_MSB + `CONF_CLK_STRETCH_F_W)

`define TX_ADDRESS_F_W `ADDR_W
`define TX_ADDRESS_MSB `TX_ADDRESS_F_W
                                        
`define TX_DIR_F_W 1
`define TX_DIR_MSB `TX_DIR_F_W
                                        
`define TX_WR_DATA_F_W `DATA_W
`define TX_WR_DATA_MSB `TX_WR_DATA_F_W
                                        
`define TX_RD_DATA_F_W `DATA_W
`define TX_RD_DATA_MSB `TX_RD_DATA_F_W
                                        
`define TX_VALID_F_W  1
`define TX_VALID_MSB  `TX_VALID_F_W
`define TX_ENABLE_F_W 1
`define TX_ENABLE_MSB (`TX_VALID_MSB + `TX_ENABLE_F_W)

`define ERROR_CODE 8'hDB
