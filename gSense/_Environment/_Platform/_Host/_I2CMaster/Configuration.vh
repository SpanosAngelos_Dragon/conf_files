// ===========================================================================
//  File          :    Configuration.vh
//  Entity        :    Configuration
//  Purpose       :    I2C Master Configuration
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 12-Oct-2013
// ===========================================================================
`include "Options.vh"
`include "StandardConfiguration.vh"
`define MODE_STANDARD_SUPPORTED
`define MODE_FAST_SUPPORTED
`define MODE_FAST_PLUS_SUPPORTED
`define MODE_HIGH_SPEED_SUPPORTED
`define CLK_STRETCHING_SUPPORTED
`define TEN_BIT_SLAVE_ADDRESS_SUPPORTED
`define GENERAL_CALL_ADDRESS_SUPPORTED
`define SOFTWARE_RESET_SUPPORTED

`define MODE_DEFAULT `MODE_STANDARD
`include "DerivedConfiguration.vh"
