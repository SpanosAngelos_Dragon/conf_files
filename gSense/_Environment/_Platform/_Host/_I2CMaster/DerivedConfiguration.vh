// ===========================================================================
//  File          :    DerivedConfiguration.vh
//  Entity        :    DerivedConfiguration
//  Purpose       :    I2C Master Derived Configuration
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 12-Oct-2013
// ===========================================================================
`define getAddressWidth() \
   `ifdef TEN_BIT_SLAVE_ADDRESS \
      10 \
   `else \
      8 \

`ifdef MODE_HIGH_SPEED_SUPPORTED
   `define CLK_DIV_COUNT_W `getSCLDiv(`MODE_HIGH_SPEED, `HOST_CLK_PERIOD)
`elsif MODE_FAST_PLUS_SUPPORTED
   `define CLK_DIV_COUNT_W `getSCLDiv(`MODE_FAST_PLUS, `HOST_CLK_PERIOD)
`elsif MODE_FAST_SUPPORTED
   `define CLK_DIV_COUNT_W `getSCLDiv(`MODE_FAST, `HOST_CLK_PERIOD)
`else
   `define CLK_DIV_COUNT_W `getSCLDiv(`MODE_STD, `HOST_CLK_PERIOD)
`endif

`define MODE_STD           0
`define MODE_W             Math::log2(`MODE_STD + 1)
`ifdef MODE_FAST_SUPPORTED
   `define MODE_FAST       (`MODE_STD + 1)
   `undef  MODE_W
   `define MODE_W          Math::log2(`MODE_FAST + 1)
`endif
`ifdef MODE_FAST_PLUS_SUPPORTED
   `define MODE_FAST_PLUS  (`MODE_FAST + 1)
   `undef  MODE_W
   `define MODE_W          Math::log2(`MODE_FAST_PLUS  + 1)
`endif
`ifdef MODE_HIGH_SPEED_SUPPORTED
   `define MODE_HIGH_SPEED (`MODE_FAST_PLUS + 1)
   `undef  MODE_W
   `define MODE_W          Math::log2(`MODE_HIGH_SPEED + 1)
`endif
