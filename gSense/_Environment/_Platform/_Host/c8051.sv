// ===========================================================================
//  File          :    c8051.v
//  Entity        :    c8051
//  Purpose       :    8051 CPU
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Jul-2013
//  Possible optimizations :
//    1. Currently the RAM bitcount is 
//          ISA_BYTE_N * 8 * RAM_PRG_DATA_N
//       However, not all instructions need ISA_BYTE_N bytes.
//       Thus we could reduce the bitcount by merging subsequent words of unequal
//       byte num.
// ===========================================================================
`include "c8051/ISA.svh"
import math::log2;
import math::max;

module c8051 #(
   parameter  RAM_EXT_DATA_W = 8,
   parameter  RAM_EXT_DATA_N = 256,
   parameter  RAM_PRG_DATA_N = 1024,
   parameter  ISR_NUM        = 2,

   localparam RAM_PRG_ADDR_W = log2(RAM_PRG_DATA_N),
   localparam RAM_EXT_ADDR_W = log2(RAM_EXT_DATA_N),
   localparam RAM_PRG_DATA_W = ISA_BYTE_N * 8,
) (
   input                          clkP,
   input                          aresetNP,
   input                          sresetNP,

   input  [ISR_NUM        -1 : 0] isrNP,
   input                          progNP,
   input  [RAM_RPG_ADDR_W -1 : 0] prgramAddrInP,
   input  [RAM_PRG_DATA_W -1 : 0] programDataInP,
   input                          programValidInP,
   output                         programEnableOutP,

   output [RAM_EXT_ADDR_W -1 : 0] addrOutP
);

`include "c8051/program.sv"
`include "c8051/fetch.sv"
`include "c8051/decode.sv"
`include "c8051/execute.sv"

wire [RAM_PRG_ADDR_W -1 : 0] fetchRAMAddr;
wire [RAM_RPG_ADDR_W -1 : 0] programAddr;
wire [RAM_PRG_DATA_W -1 : 0] programData;

assign programAddr = (progNP) ? fetchRAMAddr : addrInP;

program_0 program #(
   .DATA_W  (RAM_PRG_DATA_W),
   .DATA_N  (RAM_PRG_DATA_N)
) (
   .clkP         (clkP),
   .aresetNP     (aresetNP),
                
   .progNP       (progNP)

   .addrP        (programAddrInP),
   .dataInP      (programDataInP),
   .validInP     (programValidInP),
   .enableOutP   (programEnableOutP),
                
   .dataOutP     (programData),
   .validOutP    (programValid),
   .enableInP    (fetchEnable)
);

fetch_0 fetch #(
   .DATA_W  (RAM_PRG_DATA_W),
   .ADDR_W  (RAM_PRG_ADDR_W)
) (
   .clkP       (clkP),
   .aresetNP   (aresetNP),
   .sresetNP   (sresetNP),

   .addrP      (fetchRAMAddr),

   .addrAbsoluteP       (addrAbsoluteP),
   .addrAbsoluteValidP  (addrAbsoluteValidP),
   .addrAbsoluteEnableP (addrAbsoluteEnableP),

   .addrOffsetP       (addrOffsetP),
   .addrOffsetValidP  (addrOffsetValidP),
   .addrOffsetEnableP (addrOffsetEnableP),

   .dataInP    (programData),
   .validInP   (programValid),
   .enableOutP (fetchEnable),

   .dataOutP   (fetchData),
   .enableInP  (decodeEnable),
   .validOutP  (fetchValid),
);

decode_0 decode #(
   .DATA_W  (RAM_PRG_DATA_W)
) (
   .clkP       (clkP),
   .aresetNP   (aresetNP),

   .dataInP    (fetchData),
   .validInP   (fetchValid),
   .enableOutP (decodeEnable),

   .dataOutP   (decodeData),
   .validOutP  (decodeValid),
   .enableInP  (executeEnable)
);

execute_0 execute (
   .clkP       (clkP),
   .aresetNP   (aresetNP),

   .dataInP    (decodeData),
   .validInP   (decodeValid),
   .enableOutP (executeEnable),

   .dataOutP   (executeData),
   .validOutP  (executeValid),
   .enableInP  (enableInP)
);

endmodule
