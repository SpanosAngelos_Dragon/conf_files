//===========================================================================
//  File          :    C8051.v
//  Entity        :    C8051
//  Purpose       :    8051 CPU
//===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Jul-2013
//  Possible optimizations :
//    1. Currently the RAM bitcount is
//          ISA_BYTE_N * 8 * RAM_PRG_DATA_N
//       However, not all instructions need ISA_BYTE_N bytes.
//       Thus we could reduce the bitcount by merging subsequent words of unequal
//       byte num.
// ===========================================================================
`include "_C8051/ISA.svh"
import Math::log2;
import Math::max;

module C8051 #(
   parameter  RAM_EXT_DATA_W = 8,
   parameter  RAM_EXT_DATA_N = 256,
   parameter  RAM_PRG_DATA_N = 1024,
   parameter  ISR_NUM        = 2,

   localparam RAM_PRG_ADDR_W = log2(RAM_PRG_DATA_N),
   localparam RAM_EXT_ADDR_W = log2(RAM_EXT_DATA_N),
   localparam RAM_PRG_DATA_W = ISA_BYTE_N * 8,
) (
   input clkP,
   input aresetNP,
   input sresetNP,

   input [ISR_NUM -1 : 0] isrNP,

   `ifdef HOST_PRG_ROM_EXT,
      output [`HOST_ADDR_W -1 : 0 ] prgROMAddr,
      input  [`HOST_DATA_W -1 : 0 ] prgROMData
   `endif

   output [SB_ADDR_W -1 : 0] sbAddrP,
   output [SB_DATA_W -1 : 0] sbDataWrP,
   input  [SB_DATA_W -1 : 0] sbDataRdP,
   output sbValidWrP,
   input  sbValidRdP,
   output sbRdNwrP
);
   `include "_C8051/Decode.sv"
   Decode decode #(
      .DATA_W  (RAM_PRG_DATA_W)
   ) (
      .instructionP        (instructionP),
      .instructionValidP   (instructionValidP),
      .instructionEnableP  (instructionEnableP),

      .basicValidP      (basicValidW),
      .basicOperationP  (basicOperationW),

      .aPCValidP        (aPCValidW),
      .aPCOperationP    (aPCOperationW),

      .lPCValidP        (lPCValidW),
      .lPCOperationP    (lPCOperationW),

      .rotateValidP     (rotateValidW),
      .rotateOperationP (rotateOperationW),

      .jumpValidP       (jumpValidW),
      .jumpOperationP   (jumpOperationW),

      .NOPValidP        (NOPValidW),

      .retValidP        (retValidW),
      .retOperationP    (retOperationW),

      .bitCValidP       (bitCValidW),
      .bitCOperationP   (bitCOperationW),

      .bitValidP        (bitValidW),
      .bitOperationP    (bitOperationW),

      .cValidP          (cValidW),
      .cOperationP      (cOperationW),

      .aValidP          (aValidW),
      .aOperationP      (aOperationW),

      .movxValidP       (movxValidW),
      .movxOperationP   (movxOperationW),

      .stackValidP      (stackValidW),
      .stackOperationP  (stackOperationW),

      .movcValidP       (movcValidW),
      .movcOperationP   (movcOperationW),

      .cBitValidP       (cBitValidW),
      .cBitOperationP   (cBitOperationW),

      .extraValidP      (extraValidW),
      .extraOperationP  (extraOperationW),

      .operandP         (operandW),
      .operandOutValidP (operandOutValidW),
      .operandOutValueP (operandOutValueW),
            
      .enableInP        (enableInW)
   );

   `include "_C8051/Execute.sv"
   Execute # (
      .REGISTER_W = 8,
      .REGISTER_N = 8,
      .ACCUMULATOR_W = REGISTER_W,
      .RAM_EXT_DATA_W = 8,
      .RAM_EXT_DATA_N = 2**16,
      .RAM_PRG_DATA_N = 2**16,
      .HPATH = "",
   ) (
      .clkP       (clkP),
      .aresetNP   (aresetNP),
      .sresetNP   (sresetNP),

      .enableOutP (enableOutW),

      .aPCValidP        (aPCValidW),
      .aPCOperationP    (aPCOperationW),

      .lPCValidP        (lPCValidW),
      .lPCOperationP    (lPCOperationW),

      .rotateValidP     (rotateValidW),
      .rotateOperationP (rotateOperationW),

      .jumpValidP       (jumpValidW),
      .jumpOperationP   (jumpOperationW),

      .NOPValidP        (NOPValidW),

      .retValidP        (retValidW),
      .retOperationP    (retOperationW),

      .bitCValidP       (bitCValidW),
      .bitCOperationP   (bitCOperationW),

      .bitValidP        (bitValidW),
      .bitOperationP    (bitOperationW),

      .cValidP          (cValidW),
      .cOperationP      (cOperationW),

      .aValidP          (aValidW),
      .aOperationP      (aOperationW),

      .movxValidP       (movxValidW),
      .movxOperationP   (movxOperationW),

      .stackValidP      (stackValidW),
      .stackOperationP  (stackOperationW),

      .movcValidP       (movcValidW),
      .movcOperationP   (movcOperationW),

      .cBitValidP       (cBitValidW),
      .cBitOperationP   (cBitOperationW),

      .extraValidP      (extraValidW),
      .extraOperationP  (extraOperationW),

      .operandP      (operandW),
      .operandValueP (operandValueW),

      .sbAddrP    (sbAddrP),
      .sbDataWrP  (sbDataWrP),
      .sbDataRdP  (sbDataRdP),
      .sbValidWrP (sbValidWrP),
      .sbValidRdP (sbValidRdP),
      .sbRdNwrP   (sbRdNwrP)
   );
endmodule
