// ===========================================================================
//  File          :    execute.sv
//  Entity        :    execute
//  Purpose       :    C8051 Instruction Execution
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 02-Oct-2013
//    Need to fix :
//       RAM accesses for @R0, @R1. Currently Invalid.
//    Need to add :
//       Timers.
// ===========================================================================

`include "execute/execute_t.svh"
`include "execute/stack.svh"
`define PSW(x) (( pswR >> x ) & 1'b1)

import Math::log2;

module Execute # (
   parameter REGISTER_W = 8,
   parameter REGISTER_N = 8,
   parameter ACCUMULATOR_W = REGISTER_W,
   parameter RAM_EXT_DATA_W = 8,
   parameter RAM_EXT_DATA_N = 2**16,
   parameter RAM_PRG_DATA_N = 2**16,
   parameter HPATH = "",

   localparam RAM_EXT_ADDR_W = log2(RAM_EXT_DATA_N)
   localparam RAM_PRG_ADDR_W = log2(RAM_PRG_DATA_N)
) (
   input clkP,
   input aresetNP,
   input sresetNP,

   output enableOutP,

   input       aPCValidP,
   input [3:0] aPCOperationP,

   input lPCValidP,
   input lPCOperationP,

   input       rotateValidP,
   input [1:0] rotateOperationP,

   input       jumpValidP,
   input [3:0] jumpOperationP,

   input NOPValidP,
               
   input retValidP,
   input retOperationP,

   input       bitCValidP,
   input [1:0] bitCOperationP,

   input bitValidP,
   input bitOperationP,
               
   input cValidP,
   input cOperationP,

   input       aValidP,
   input [1:0] aOperationP,

   input       movxValidP,
   input [2:0] movxOperationP, 

   input stackValidP,
   input stackOperationP,
               
   input movcValidP,
   input movcOperationP,
               
   input cBitValidP,
   input cBitOperationP,
               
   input extraValidP,
   input extraOperationP,

   input [`ISA_OPERAND_GROUP_MEMBER_W     -1 : 0 ] operandP,
   input [`ISA_OPERAND_W * `ISA_OPERAND_N -1 : 0 ] operandValueP,

   output [SB_ADDR_W -1 : 0] sbAddrP,
   output [SB_DATA_W -1 : 0] sbDataWrP,
   input  [SB_DATA_W -1 : 0] sbDataRdP,
   output sbValidWrP,
   input  sbValidRdP,
   output sbRdNwrP
);

//===========================================================================
// Stack Interface
//===========================================================================
reg                               stackDoubleWordOpR;
reg  [log(`STACK_OP_N)    -1 : 0] stackOpR;
reg  [`STACK_MAX_BYTE * 8 -1 : 0] stackDataInR;
wire [`STACK_MAX_BYTE * 8 -1 : 0] stackDataOutW;

//===========================================================================
// Register Bank Interface
//===========================================================================
reg [log2(REGISTER_N) -1 : 0] registerIndexR;
reg [     REGISTER_W  -1 : 0] registerValueNewR;
wire[     REGISTER_W  -1 : 0] registerValueCurW;

//===========================================================================
// RAM Interface
//===========================================================================
reg [RAM_EXT_DATA_W -1 : 0] ramWrDataR;
reg                         ramCER;
reg                         ramWrR;
reg [RAM_EXT_ADDR_W -1 : 0] ramAddrR;

//===========================================================================
// Status registers
//===========================================================================
reg [ACCUMULATOR_W    : 0] aR;
reg [PSW_1         -1 : 0] pswR;
reg [PC_W          -1 : 0] pcR;
reg [ACCUMULATOR_W -1 : 0] dptrR
reg                        interruptFR;

//===========================================================================
// Operands Split
//===========================================================================
wire [`ISA_OPERAND_W -1 : 0] operandValueW [`ISA_OPERAND_N -1 : 0];
for ( int operandIdx = 0; operandIdx < `ISA_OPERAND_N; operandIdx++) begin
   assign operandValueW[operandIdx] = operandValueP[(operandIdx+1) * `ISA_OPERAND_W -1 -: `ISA_OPERAND_W];
end

registerBank RegisterBank #(
   .REGISTER_N      (REGISTER_N),
   .REGISTER_W      (REGISTER_W)
) (                
   .clkP              (clkP),
   .aresetNP          (aresetNP),
   .addrP             (operandP),
   .wr                (registerWrR),
   .registerValueInP  (registerValueNewR),
   .registerValueOutP (registerValueCurW)
);

always @ (posedge clkP) begin
   directImmed_t directImmedV;
   directImmedV.direct = operandValueW[0];
   directImmedV.immed  = operandValueW[1];
   ramCER <= 1'b0;
   ramWrR <= 1'b0;
   if (sbValidRdP) begin
      ramCER <= 1'b1;
      ramWrR <= 1'b1;
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_INC : begin
               ramWrDataR <= sbDataRdP + 1;
            end
            `GROUP_BASIC_DEC : begin
               ramWrDataR <= sbDataRdP -1 ;
            end
            `GROUP_BASIC_MOVRD : begin
               ramWrDataR <= sbDataRdP;
               ramAddrR   <= registerValueCurW;
            end
            `GROUP_BASIC_XCH : begin
               ramWrDataR <= aR;
            end
            `GROUP_BASIC_DJNZDO : begin
               if (operand0TypeP == `OPERAND_R0_ADDR &&
                   operand0TypeP == `OPERAND_R1_ADDR
               ) begin
                  ramWrDataR <= {ramRdDataR[7:4], aR[3:0]};
               end
               else
                  ramWrDataR <= ramRdDataR - 1;
               end
            end
         endcase
      end
   end
   else if (operand0TypeP == `OPERAND_DIRECT_A) begin
      ramAddrR <= directImmedV.direct;
      ramCER   <= 1'b1;
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_ORL : begin
               ramWrDataR <= |aR;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_ANL : begin
               ramWrDataR <= &aR;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_XRL : begin
               ramWrDataR <= ^aR;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_SUBB : begin
               ramWrR   <= 1'b0;
               ramAddrR <= operandValueW[0];
            end
            `GROUP_BASIC_CJNERIO : begin
               ramWrR  <= 1'b0;
               ramAddrR <= operandValueW[0];
            end
            `GROUP_BASIC_XCH : begin
               ramWrR <= 1'b0;
               ramAddrR <= operandValueW[0];
            end
         endcase
      end
   end
   else if (operand0TypeP == `OPERAND_DIRECT_IMMED) begin
      ramAddrR <= directImmedV.direct;
      ramCER   <= 1'b1;
      ramWrR   <= 1'b1;
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_ORL : begin
               ramWrDataR <= |operandValueW[0];
            end
            `GROUP_BASIC_ANL : begin
               ramWrDataR <= &operandValueW[0];
            end
            `GROUP_BASIC_XRL : begin
               ramWrDataR <= ^operandValueW[0];
            end
            `GROUP_BASIC_MOVRI : begin
               ramWrDataR <= operandValueW[0];
            end
         endcase
      end
   end
   else if ((operand0TypeP == `OPERAND_R0_ADDR) || 
            (operand0TypeP == `OPERAND_R1_ADDR)
   ) begin
      ramAddrR <= registerValueCurW;
      ramCER   <= 1'b1;
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_INC : begin
               ramWrDataR <= sbDataRdP + 1;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_DEC : begin
               ramWrDataR <= sbDataRdP -1 ;
               ramWrR   <= 1'b1;
            end
            `GROUP_BASIC_MOV : begin
               ramWrDataR <= sbDataRdP;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_MOVDR : begin
               ramWrDataR <= sbDataRdP;
               ramWrR     <= 1'b1;
            end
            `GROUP_BASIC_SUBB : begin
               ramAddrR <= registerValueCurW;
               ramWrR   <= 1'b0;
            end
            `GROUP_BASIC_MOVRD : begin
               ramAddrR <= operandValueW[0];
               ramWrR   <= 1'b0;
            end
            `GROUP_BASIC_XCH : begin
               ramAddrR <= registerValueCurW;
               ramWrR   <= 1'b0;
            end
            `GROUP_BASIC_DJNZDO : begin
               ramAddrR <= registerValueCurW[0];
               ramWrR   <= 1'b0;
            end
         endcase
      end
   end
   else if (operand0TypeP == `OPERAND_DIRECT) begin
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_DJNZDO : begin
               ramAddrR <= operandValueW[0];
               ramWrR   <= 1'b0;
            end
            `GROUP_BASIC_MOVAR : begin
               ramAddrR <= operandValueW[0];
               ramWrR   <= 1'b0;
            end
            `GROUP_BASIC_MOVRA : begin
               ramAddrR <= operandValueW[0];
               ramWrR   <= 1'b0;
         endcase
      end
   end
   else if (operand0TypeP >= `OPERAND_R0) begin
      ramAddrR <= registerValueCurW;
      ramCER   <= 1'b1;
      if (basicValidP) begin
         case (basicOperationP) begin
            `GROUP_BASIC_MOVDR : begin
               ramWrDataR <= sbDataRdP;
            end
         encase
      end
   end
   else if (movcValidP) begin
      ramCER <= 1'b1;
      case (movcOperationP) begin
         `GROUP_MOVC_AP : begin
            ramAddrR <= aR + pcR + 1;
         `GRUOP_MOVC_AD : begin
            ramAddrR <= aR + dptrR;
         end
      endcase
   end
   else if (movxValidP) begin
      ramCER <= 1'b1;
      case (movxOperationP) begin
         `GROUP_MOVX_AP : begin
            case (operand0TypeP) begin
               `OPERAND_MOVX_DPTR : begin
                  ramAddrR <= dptrR;
               end
               `OPERAND_MOVX_R0_ADDR : begin
                  ramAddrR <= registerValueCurW;
               end
               `OPERAND_MOVX_R1_ADDR : begin
                  ramAddrR <= registerValueCurW;
               end
            endcase
         end
         `GROUP_MOVX_PA : begin
            case (operand0TypeP) begin
               `OPERAND_MOVX_DPTR : begin
                  ramAddrR   <= dptrR;
                  ramWrDataR <= aR;
                  ramWrR     <= 1'b1;
               end
               `OPERAND_MOVX_R0_ADDR : begin
                  ramAddrR <= registerValueCurW;
                  ramWrDataR <= aR;
                  ramWrR     <= 1'b1;
               end
               `OPERAND_MOVX_R1_ADDR : begin
                  ramAddrR <= registerValueCurW;
                  ramWrDataR <= aR;
                  ramWrR     <= 1'b1;
               end
            endcase
         end
      endcase
end

always @ (posedge clkP or negedge aresetNP) begin
   if (!aresetNP) begin
      stackOpR <= `STACK_OP_NONE;
   end
   else if ((aPCValidP) && (aPCOperationP == `GROUP_A_PC_ACALL) ||
            (lPCValidP) && (lPCOperationP == `GROUP_L_PC_LCALL)
   ) begin
         stackOpR           <= `STACK_OP_WR;
         stackDoubleWordOpR <= 1'b1;
         stackDataInR       <= pcR + 2;
      end
   end
   else if (retValidP) begin
      stackOpR           <= `STACK_OR_RD;
      stackDoubleWordOpR <= 1'b1;
   end
   else if (stackValidP) begin
      case (stackOperationP) begin
         `GROUP_STACK_PUSH : begin
            stackOpR           <= `STACK_OP_WR;
            stackDoubleWordOpR <= 1'b0;
            stackDataInR       <= aR;
         end
         `GROUP_STACK_POP : begin
            stackOpR           <= `STACK_OP_WR;
            stackDoubleWordOpR <= 1'b0;
         end
         default : begin
            warnInvalidOp( "STACK", stackOperationP );
         end
      endcase
   end
   else begin
      stackOpR <= `STACK_OP_NONE;
   end
end

assign sbDataWrP = ramWrDataR;

always @ (posedge clkP or negedge aresetNP) begin
   if (!aresetNP) begin
      aR <= 0 ;
   end
   else if (basicValidP) begin
      if (operand0TypeP == `OPERAND_IMMED_A) begin
         case (basicOperationP) begin
            `GROUP_BASIC_INC : begin
               aR <= aR + 1;
            end
            `GROUP_BASIC_DEC : begin
               aR <= aR - 1; 
            end
            `GROUP_BASIC_ADD : begin
               aR <= aR + operandValueW[0];
            end
            `GROUP_BASIC_ADDC : begin
               aR <= aR + operandValueW[0];
            end
            `GROUP_BASIC_ORL : begin
               aR <= |operandValueW[0];
            `GROUP_BASIC_ANL : begin
               aR <= &operandValueW[0];
            end
            `GROUP_BASIC_XRL : begin
               aR <= ^operandValueW[0];
            end
            `GROUP_BASIC_MOVRI : begin
               aR <= operandValueW[0];
            end
            `GROUP_BASIC_SUBB : begin
               aR <= aR - operandValueW[0];
            end
         endcase
      else if (operand0TypeP == `OPERAND_DIRECT  || 
               operand0TypeP == `OPERAND_R0_ADDR || 
               operand0TypeP == `OPERAND_R1_ADDR)
      begin
         case(basicOperationP) begin
            `GROUP_BASIC_ADD : begin
               aR <= aR + sbDataRdP;
            end
            `GROUP_BASIC_ADDC : begin
               aR <= aR + sbDataRdP;
            end
            `GROUP_BASIC_ANL : begin
               aR <= &sbDataRdP;
            end
            `GROUP_BASIC_ORL : begin
               aR <= |sbDataRdP;
            end
            `GROUP_BASIC_XRL : begin
               aR <= ^sbDataRdP;
            end
            `GROUP_BASIC_SUBB : begin
               aR <= aR - sbDataRdP;
            end
            `GROUP_BASIC_XCH : begin
               aR <= sbDataRdP;
            end
            `GROUP_BASIC_DJNZDO : begin
               aR[3:0] <= sbDataRdP[3:0];
            end
         endcase
      end
      else if (operand0TypeP >= `OPERAND_R0) begin
         case (basicOperationP) begin
            `GROUP_BASIC_ADD : begin
               aR <= registerValueCurW + aR;
            end
            `GROUP_BASIC_ADDC : begin
               aR <= registerValueCurW + aR;
            end
            `GROUP_BASIC_ORL : begin
               aR <= |registerValueCurW;
            end
            `GROUP_BASIC_ANL : begin
               aR <= &registerValueCurW;
            end
            `GROUP_BASIC_XRL : begin
               aR <= ^registerValueCurW;
            end
            `GROUP_BASIC_SUBB : begin
               aR <= aR - registerValueCurW;
            end
            `GROUP_BASIC_XCH : begin
               aR <= registerValueCurW;
            end
         endcase
         `GROUP_BASIC_ORL : begin
      endcase
   end
   else if (rotateValidP) begin
      case (rotateOperationP) begin
         `GROUP_ROTATE_RR : begin
            aR <= aR >> 1;
         end
         `GROUP_ROTATE_RRC : begin
            aR <= aR >> 1;
         end
         `GROUP_ROTATE_RL : begin
            aR <= aR << 1;
         end
         `GROUP_ROTATE_RLC : begin
            aR <= aR << 1;
         end
         default : warnInvalidOp ( "ROTATE", rotateOperationP );
      endcase
   end
   else if (movcValidP) begin
      aR <= sbDataRdP;
   end
   else if (stackValidP) begin
      case (stackOperationP) begin
         `GROUP_STACK_POP : begin
            aR <= stackDataOutW[7:0];
         end
      endcase
   end
   else if (aValidP) begin
      case (aOperationP) begin
         `GROUP_A_SWAP : begin
            aR[3:0] <= aR[7:4];
            aR[7:4] <= aR[3:0];
         end
         `GROUP_A_DA : begin
            warnInvalidOp( "GROUP_A", `GROUP_A_DA);
         end
         `GROUP_A_CLR : begin
            aR <= 0;
         end
         `GROUP_A_CPL : begin
            aR <= ~aR;
         end
      endcase
   end
   else if (movxValidP) begin
      case (movxOperationP) begin
         `GROUP_MOVX_AP : begin
            aR <= sbDataRdP;
         end
      endcase
   end
end

always @ (posedge clkP or negedge aresetNP) begin
   if (!aresetNP) begin
      pswR <= 0;
   end
   else if (basicValidP) begin
      case (basicOperationP) begin
         `GROUP_BASIC_ADDC : begin
            pswR[`PSW_CY] <= aR[$size(aR) -1 : 0];
         end
      endcase
   end
   else if (bitCValidP) begin
      case (bitCOperationP) begin
         `GROUP_BIT_C_ORLCB : begin
            pswR[`PSW_CY] <= pswR[`PSW_CY] | PSW(operandValueW[0]);
         end
         `GROUP_BIT_C_ANLCB : begin
            pswR[`PSW_CY] <= pswR[`PSW_CY] & PSW(operandValueW[0]);
         end
         `GROUP_BIT_C_MOVBC : begin
            pswR[`PSW_CY] <= PSW(operandValueW[0]);
         end
         `GROUP_BIT_C_MOVCB : begin
            pswR <= pswR | (pswR[`PSW_CY] << operandValueW[0]);
         end
      endcase
   end
   else if (bitValidP) begin
      case (bitOperationP) begin
         `GROUP_BIT_CPLB : begin
            pswR <= pswR ^ (1'b1 << operandValueW[0]);
         end
         `GRUOP_BIT_CLRB : begin
            pswR <= pswR & !(1'b1 << operandValueW[0]);
         end
         `GROUP_BIT_SETB : begin
            pswR <= pswR | (1'b1 << operandValueW[0]);
         end
      endcase
   end
   else if (cValidP) begin
      case (cOperationP) begin
         `GROUP_C_CPL : begin
            pswR[`PSW_CY] <= ~pswR[`PSW_CY];
         end
         `GROUP_C_CLR : begin
            pswR[`PSW_CY] <= 1'b0;
         end
         `GROUP_C_CLR : begin
            pswR[`PSW_CY] <= 1'b1;
         end
      endcase
   end
   else if (rotateValidP) begin
      case (rotateOperationP) begin
         `GROUP_ROTATE_RLC : begin
            pswR[`PSW_CY] <= aR[$size(aR) -1 : 0];
         end
         `GROUP_ROTATE_RRC : begin
            pswR[`PSW_CY] <= aR[0];
         end
      endcase
   end
end


always @ (posedge clkP) begin
   bitOffset_t  bitOffsetV;
   logic [ 7:0] offsetV;
   logic [10:0] addr11V;
   logic [15:0] addr16V;
   bitOffsetV.bitIndex =  operandValueW[0];
   bitOffsetV.offset   =  operandValueW[1];
   offsetV             =  operandValueW[0];
   addr11V             = {operandValueW[1][2:0], operandValueW[0]};
   addr16V             = {operandValueW[1],      operandValueW[0]};
   if (jumpValidP) begin
      case (jumpOperationP) begin
         `GROUP_JUMP_JBC : begin
            if (`PSW(bitOffsetV.bitIndex)) begin
               pcR <= pcR + bitOffsetV.offset;
               pswR[bitOffsetV.bitIndex] = 1'b0; 
            end
          end
          `GROUP_JUMP_JB : begin
            if (~`PSW(bitOffset.bitIndex)) begin
               pcR <= pcR + bitOffsetV.offset;
            end
          end
          `GROUP_JUMP_JC : begin
            if (`PSW(`PSW_CY)) begin
               pcR <= pcR + bitOffsetV.offset;
            end
         end
         `GROUP_JUMP_JNC : begin
            if (~`PSW(`PSW_CY)) begin
               pcR <= pcR + bitOffsetV.offset;
            end
         end
         `GROUP_JUMP_JZ : begin
            if (!aR) begin
               pcR <= pcR + bitOffsetV.offset;
            end
         end
         `GROUP_JUMP_JNZ : begin
            if (aR) begin
               pcR <= pcR + bitOffsetV.offset;
            end
         end
         `GROUP_JUMP_SJMP : begin
            pcR <= pcR + bitOffsetV.offset;
         end
         default : warnInvalidOp ( "JUMP", jumpOperationP );
      endcase
   end
   else if (retValidP) begin
      case (retOperationP) begin
         `RET : begin
            pcR                <= stackOutP[15:0];
         `RETI : begin
            pcR                <= stackOutP[15:0];
            interruptFR        <= 1'b0;
         end
         default : warnInvalidOp( "RETURN", retOperationP );
      endcase
   end
   else if (aPCValidP) begin
      pcR <= addr11V;
   end
   else if (lPCValidP) begin
      pcR <= addr16V;
   end
   else if (basicValidP) begin
      case (basicOperationP) begin
         `GROUP_BASIC_CJNERIO : begin
            case ( operand0TypeP ) begin
               `OPERAND_IMMED_A : begin
                  if (aR == operandValueW[0]) begin
                     pcR <= pcR + operandValueW[1];
                  end
               end
               `OPERAND_DIRECT : begin
                  if (sbValidRdP) begin
                     if (aR == sbDataRdP) begin
                        pcR <= pcR + operandValueW[1];
                     end
                  end
               end
               `OPERAND_R0_ADDR : begin
                  if (sbValidRdP) begin
                     if (aR == sbDataRdP) begin
                        pcR <= pcR + operandValueW[1];
                     end
                  end
               end
               `OPERAND_R1_ADDR : begin
                  if (sbValidRdP) begin
                     if (aR == sbDataRdP) begin
                        pcR <= pcR + operandValueW[1];
                     end
                  end
               end
               default : begin
                  if (registerValueCurW == operandValueW[0]) begin
                     pcR <= pcR + operandValueW[1];
                  end
               end
            endcase
         end
         `GROUP_BASIC_DJNZDO : begin
            if (operand0TypeP != `OPERAND_R0_ADDR &&
                operand0TypeP != `OPERAND_R1_ADDR
            ) begin
               if (registerValueCurW -1 == 0) begin   
                  pcR <= pcR + operandValueW[0];
               end
            end
         end
      endcase
   end
end

always @ (posedge clk) begin
   registerWrR <= 1'b0;
   if (basicValidP) begin
      if (operandP >= `OPERAND_R0) begin
         case (basicOperationP) begin
            `GROUP_BASIC_INC : begin
               registerValueNewR <= registerValueCurW + 1;
               registerWrR       <= 1'b1;
            end
            `GROUP_BASIC_DEC : begin
               registerValueNewR <= registerValueCurW - 1;
            end
            `GROUP_BASIC_MOVRI : begin
               registerValueNewR <= operandValueW[0];
            end
            `GROUP_BASIC_MOVRA : begin
               registerValueNewR <= aR[$size(registerValueNewR) -1 : 0];
            end
            `GROUP_BASIC_XCH : begin
               registerValueNewR <= aR;
               registerWrR       <= 1'b1;
            end
         endcase
      end
      else if (operandP == `OPERAND_DIRECT) begin
         case (basicOperationP) begin
            `GROUP_BASIC_MOVRD : begin
               registerValueNewR <= sbDataRdP;
               registerWrR       <= 1'b1;
            end
         endcase
      end
      else if (operandP != `OPERAND_R0_ADDR &&
               operandP != `OPERAND_R1_ADDR
      ) begin
         registerValueNewR <= registerValueCurW - 1;
      end
   end
end

`ifndef SYNTHESIS
   function warnInvalidOp( string operationGroup, int operationMember );
      static string blockFullpath = {HPATH, "execute "};
      $display( "ERR::%s::Unknown %s(%d)", blockFullpath, operationGroup, operationMember );
   endfunction
`endif

endmodule
