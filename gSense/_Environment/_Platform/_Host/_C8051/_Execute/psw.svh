// ===========================================================================
//  File          :    psw.svh
//  Entity        :    psw
//  Purpose       :    Program Status Word.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Oct-2013
// ===========================================================================
`define PSW_P   0              // Even parity flag
`define PSW_RSV (`PSW_P   + 1) // Reserved
`define PSW_OV  (`PSW_RSV + 1) // Overflow flag
`define PSW_RS0 (`PSW_OV  + 1) // Register bank select 0
`define PSW_RS1 (`PSW_RS0 + 1) // Register bank select 1
`define PSW_F0  (`PSW_RS1 + 1) // Flag 0
`define PSW_AC  (`PSW_F0  + 1) // Auxiliary carry flag
`define PSW_CY  (`PSW_AC  + 1) // Carry Flag
`define PSW_W   (`PSW_CY  + 1)
