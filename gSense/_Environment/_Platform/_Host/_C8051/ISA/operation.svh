// ===========================================================================
//  File          :    operation.svh
//  Entity        :    operation
//  Purpose       :    ISA Type
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 29-Sep-2013
// ===========================================================================
`define GROUP_BASIC                   0
`define GROUP_BASIC_INC               'h00
`define GROUP_BASIC_START             `GROUP_BASIC_INC
`define GROUP_BASIC_DEC               (`GROUP_BASIC_INC     + 1)
`define GROUP_BASIC_ADD               (`GROUP_BASIC_DEC     + 1)
`define GROUP_BASIC_ADDC              (`GROUP_BASIC_ADD     + 1)
`define GROUP_BASIC_ORL               (`GROUP_BASIC_ADDC    + 1)
`define GROUP_BASIC_ANL               (`GROUP_BASIC_ORL     + 1)
`define GROUP_BASIC_XRL               (`GROUP_BASIC_ANL     + 1)
`define GROUP_BASIC_MOVRI             (`GROUP_BASIC_XRL     + 1)
`define GROUP_BASIC_MOVDR             (`GROUP_BASIC_MOVRI   + 1)
`define GROUP_BASIC_SUBB              (`GROUP_BASIC_MOVDR   + 1)
`define GROUP_BASIC_MOVRD             (`GROUP_BASIC_SUBB    + 1)
`define GROUP_BASIC_CJNERIO           (`GROUP_BASIC_MOVRD   + 1)
`define GROUP_BASIC_XCH               (`GROUP_BASIC_CJNERIO + 1)
`define GROUP_BASIC_DJNZDO            (`GROUP_BASIC_XCH     + 1)
`define GROUP_BASIC_MOVAR             (`GROUP_BASIC_DJNZDO  + 1)
`define GROUP_BASIC_MOVRA             (`GROUP_BASIC_MOVAR   + 1)
`define GROUP_BASIC_END               `GROUP_BASIC_MOVAD
`define GROUP_BASIC_MEMBER_N          (`GROUP_BASIC_START - `GROUP_BASIC_END + 1)
`define GROUP_BASIC_MAX_MEMBER_N      `GROUP_BASIC_MEMBER_N

`define GROUP_A_PC                    (`GROUP_BASIC + 1)
`define GROUP_A_PC_AJMP               'h00
`define GROUP_A_PC_START              `GROUP_A_PC_AJMP
`define GROUP_A_PC_ACALL              (`GROUP_A_PC_AJMP   + 1)
`define GROUP_A_PC_AJMP1              (`GROUP_A_PC_ACALL  + 1)
`define GROUP_A_PC_ACALL1             (`GROUP_A_PC_AJMP1  + 1)
`define GROUP_A_PC_AJMP2              (`GROUP_A_PC_ACALL1 + 1)
`define GROUP_A_PC_ACALL2             (`GROUP_A_PC_AJMP2  + 1)
`define GROUP_A_PC_AJMP3              (`GROUP_A_PC_ACALL2 + 1)
`define GROUP_A_PC_ACALL3             (`GROUP_A_PC_AJMP3  + 1)
`define GROUP_A_PC_AJMP4              (`GROUP_A_PC_ACALL3 + 1)
`define GROUP_A_PC_ACALL4             (`GROUP_A_PC_AJMP4  + 1)
`define GROUP_A_PC_AJMP5              (`GROUP_A_PC_ACALL4 + 1)
`define GROUP_A_PC_ACALL5             (`GROUP_A_PC_AJMP5  + 1)
`define GROUP_A_PC_AJMP6              (`GROUP_A_PC_ACALL5 + 1)
`define GROUP_A_PC_ACALL6             (`GROUP_A_PC_AJMP6  + 1)
`define GROUP_A_PC_AJMP7              (`GROUP_A_PC_ACALL6 + 1)
`define GROUP_A_PC_ACALL7             (`GROUP_A_PC_AJMP7  + 1)
`define GROUP_A_PC_END                `GROUP_A_PC_ACALL7
`define GROUP_A_PC_MEMBER_N           (`GROUP_A_PC_AJMP - `GROUP_A_PC_END + 1)
`define GROUP_A_PC_MAX_MEMBER_N       Math::max(`GROUP_A_PC_MEMBER_N, `GROUP_BASIC_MAX_MEMBER_N)

`define GROUP_L_PC                    (`GROUP_A_PC + 1)
`define GROUP_L_PC_LJMP               'h00
`define GROUP_L_PC_START              `GROUP_L_PC_LJMP
`define GROUP_L_PC_LCALL              (`GROUP_L_PC_LJMP  + 1)
`define GROUP_L_PC_END                `GROUP_L_PC_LCALL
`define GROUP_L_PC_MEMBER_N           (`GROUP_L_PC_END - `GROUP_L_PC_START + 1)
`define GROUP_L_PC_MAX_MEMBER_N       Math::max(`GROUP_A_PC_MAX_MEMBER_N. `GROUP_L_PC_MEMBER_N)

`define GROUP_ROTATE                  (`GROUP_L_PC + 1)
`define GROUP_ROTATE_RR               'h00
`define GROUP_ROTATE_START            `GROUP_ROTATE_RR
`define GROUP_ROTATE_RRC              (`GROUP_ROTATE_RR  + 1)
`define GROUP_ROTATE_RL               (`GROUP_ROTATE_RRC + 1)
`define GROUP_ROTATE_RLC              (`GROUP_ROTATE_RL  + 1)
`define GROUP_ROTATE_END              `GROUP_ROTATE_RLC
`define GROUP_ROTATE_MEMBER_N         (`GROUP_ROTATE_END - `GROUP_ROTATE_START + 1)
`define GROUP_ROTATE_MAX_MEMBER_N     Math::max(`GROUP_L_PC_MAX_MEMBER_N, `GROUP_ROTATE_MEMBER_N)

`define GROUP_JUMP                    (`GROUP_ROTATE + 1)
`define GROUP_JUMP_JBC                'h01
`define GROUP_JUMP_START              `GROUP_JUMP_JBC
`define GROUP_JUMP_JB                 (`GROUP_JUMP_JBC  + 1)
`define GROUP_JUMP_JNB                (`GROUP_JUMP_JB   + 1)
`define GROUP_JUMP_JC                 (`GROUP_JUMP_JNB  + 1)
`define GROUP_JUMP_JNC                (`GROUP_JUMP_JC   + 1)
`define GROUP_JUMP_JZ                 (`GROUP_JUMP_JNC  + 1)
`define GROUP_JUMP_JNZ                (`GROUP_JUMP_JZ   + 1)
`define GROUP_JUMP_SJMP               (`GROUP_JUMP_JNZ  + 1)
`define GROUP_JUMP_END                (`GROUP_JUMP_SJMP + 1)
`define GROUP_JUMP_MEMBER_N           (`GROUP_JUMP_END - `GROUP_JUMP_START + 1)
`define GROUP_JUMP_MAX_MEMBER_N       Math::max(`GROUP_ROTATE_MAX_MEMBER_N, `GROUP_JUMP_MEMBER_N)

`define GROUP_NOP                     (`GROUP_JUMP + 1)
`define NOP                           'h00
`define GROUP_NOP_START               `NOP
`define GROUP_NOP_END                 `NOP
`define GROUP_NOP_MEMBER_N            (`GROUP_NOP_END - `GROUP_NOP_START + 1)
`define GROUP_NOP_MAX_MEMBER_N        Math::max(`GROUP_JUMP_MAX_MEMBER_N, `GROUP_NOP_MEMBER_N)

`define GROUP_RET                     (`GROUP_NOP + 1)
`define GROUP_RET_RET                 'h02
`define GROUP_RET_START               `GROUP_RET_RET
`define GROUP_RET_RETI                (`GROUP_RET_RET + 1)
`define GROUP_RET_END                 `GROUP_RET_RETI
`define GROUP_RET_MEMBER_N            (`GROUP_RET_END - `GROUP_RET_START + 1)
`define GROUP_RET_MAX_MEMBER_N        Math::max(`GROUP_NOP_MAX_MEMBER_N, `GROUP_RET_MEMBER_N)

`define GROUP_BIT_C                   (`GROUP_RET + 1)
`define GROUP_BIT_C_ORLCB             'h07
`define GROUP_BIT_C_START             `GROUP_BIT_C_ORLCB
`define GROUP_BIT_C_ANLBC             (`GROUP_BIT_C_ORLCB + 1)
`define GROUP_BIT_C_MOVBC             (`GROUP_BIT_C_ANLCB + 1)
`define GROUP_BIT_C_MOVCB             (`GROUP_BIT_C_MOVBC + 1)
`define GROUP_BIT_C_END               `GROUP_BIT_C_MOVCB
`define GROUP_BIT_C_MEMBER_N          (`GROUP_BIT_C_END - `GROUP_BIT_C_START + 1)
`define GROUP_BIT_C_MAX_MEMBER_N      Math::max(`GROUP_RET_MAX_MEMBER_N, `GROUP_BIT_C_MEMBER_N)

`define GROUP_BIT                     (`GROUP_BIT_C + 1)
`define GROUP_BIT_CPLB                'h0B
`define GROUP_BIT_START               `GROUP_BIT_CPLB
`define GROUP_BIT_CLRB                (`GROUP_BIT_CPLB + 1)
`define GROUP_BIT_SETB                (`GROUP_BIT_CLRB + 1)
`define GROUP_BIT_END                 (`GROUP_BIT_SETB)
`define GROUP_BIT_MEMBER_N            (`GROUP_BIT_END - `GROUP_BIT_START + 1)
`define GROUP_BIT_MAX_MEMBER_N        Math::max(`GROUP_BIT_C_MEMBER_N, `GROUP_BIT_G

`define GROUP_C                       (`GROUP_BIT + 1)
`define GROUP_C_CPL                   'h0B
`define GROUP_C_START                 `GROUP_C_CPL
`define GROUP_C_CLR                   (`GROUP_C_CPL   + 1)
`define GROUP_C_SETB                  (`GROUP_C_START + 1)
`define GROUP_C_END                   (`GROUP_C_SETC)
`define GROUP_C_MEMBER_N              (`GROUP_C_END - `GROUP_C_START + 1)
`define GROUP_C_MAX_MEMBER_N          Math::max(`GROUP_BIT_MAX_MEMBER_N, `GROUP_C_MEMBER_N)

`define GROUP_A                       (`GROUP_C + 1)
`define GROUP_A_SWAP                  'h04
`define GROUP_A_START                 `GROUP_A_SWAP
`define GROUP_A_DA                    (`GROUP_A_SWAP + 1)
`define GROUP_A_CLR                   (`GROUP_A_DA + 1)
`define GROUP_A_CPL                   (`GROUP_A_CLR + 1)
`define GROUP_A_END                   `GROUP_A_CPL
`define GROUP_A_MEMBER_N              (`GROUP_A_END - `GROUP_A_START + 1)
`define GROUP_A_MAX_MEMBER_N          Math::max(`GROUP_C_MAX_MEMBER_N, `GROUP_A_MEMBER_N)

`define GROUP_MOVX                    (`GROUP_A + 1)
`define GROUP_MOVX_AP                 'h0e
`define GROUP_MOVX_START              `GROUP_MOVX_AP
`define GROUP_MOVX_PA                 (`GROUP_MOVX_AP + 1)
`define GROUP_MOVX_END                `GROUP_MOVX_PA
`define GROUP_MOVX_MEMBER_N           (`GROUP_MOVX_START - `GROUP_MOVX_END + 1)
`define GROUP_MOVX_MAX_MEMBER_N       Math::max(`GROUP_MOVX_MEMBER_N, `GROUP_A_MAX_MEMBER_N)

`define GROUP_STACK                   (`GROUP_MOVX + 1)
`define GROUP_STACK_PUSH              'h00
`define GROUP_STACK_START             `GROUP_STACK_PUSH
`define GROUP_STACK_POP               (`GROUP_STACK_PUSH + 1)
`define GROUP_STACK_END               `GROUP_STACK_POP
`define GROUP_STACK_MEMBER_N          (`GROUP_STACK_END - `GROUP_STACK_START + 1)
`define GROUP_STACK_MAX_MEMBER_N      Math::max(`GROUP_MOVX_MAX_MEMBER_N, `GROUP_STACK_MEMBER_N)

`define GROUP_MOVC                    (`GROUP_STACK + 1)
`define GROUP_MOVC_AP                 'h3
`define GROUP_MOVC_START              `GROUP_MOVC_AP
`define GROUP_MOVC_AD                 (`GROUP_MOVC_AP + 1)
`define GROUP_MOVC_END                `GROUP_MOVC_AD
`define GROUP_MOVC_MEMBER_N           (`GROUP_MOVC_END - `GROUP_MOVC_START + 1)
`define GROUP_MOVC_MAX_MEMBER_N

`define GROUP_C_BIT                   (`GROUP_MOVC + 1)
`define GROUP_C_BIT_ORL               'h2
`define GROUP_C_BIT_START             `GROUP_C_BIT_ORL
`define GROUP_C_BIT_ANL               (`GROUP_C_BIT_ORL + 1)
`define GROUP_C_BIT_MEMBER_N          (`GROUP_C_BIT_END - `GROUP_C_BIT_START + 1)
`define GROUP_C_BIT_MAX_MEMBER_N      Math::max(`GROUP_C_BIT_MEMBER_N, `GROUP_STACK_MAX_MEMBER_N)

`define GROUP_EXTRA                   (`GROUP_EXTRA_C_BIT + 1)
`define GROUP_EXTRA_MUL               'h4
`define GROUP_EXTRA_MUL_START         `GROUP_EXTRA_MUL
`define GROUP_EXTRA_DIV               (`GROUP_EXTRA_MUL + 1)
`define GROUP_EXTRA_MUL_END           `GROUP_EXTRA_MUL
`define GROUP_EXTRA_MUL_MEMBER_N      (`GROUP_EXTRA_MUL_EXTRA_END - `GROUP_EXTRA_MUL_EXTRA_START + 1)
`define GROUP_EXTRA_MUL_MAX_MEMBER_N  Math::max(`GROUP_EXTRA_MUL_EXTRA_MEMBER_N, `GROUP_EXTRA_MUL_C_BIT_MAX_MEMBER_N)

//`define GROUP_INC_DPTR
//`define GROUP_MOV_DPTR
//`define GROUP_JMP_DPTR

`define GROUP_N         (`GROUP_EXTRA + 1)
`define MEMBER_N        `GROUP_EXTRA_MUL_MAX_MEMBER_N
