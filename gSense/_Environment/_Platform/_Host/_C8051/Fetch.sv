// ===========================================================================
//  File          :    fetch.sv
//  Entity        :    fetch
//  Purpose       :    c8051 Fetch.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 29-Sep-2013
//  Things for implementation:
//    Logic that controls the valid out in the case of an address change.
// ===========================================================================
import math::log2;

module fetch # (
   parameter DATA_W  = 24,
   parameter ADDR_W  = 8
) (
   input clkP,
   input aresetNP,
   input sresetNP,

   output [ADDR_W -1 : 0] addrP;

   input [ADDR_W -1 : 0] addrAbsoluteP;
   input                 addrAbsoluteValidP;
   output                addrAbsoluteEnableP;

   input [ADDR_W -1 : 0] addrOffsetP;
   input                 addrOffsetValidP;
   output                addrOffsetEnableP;

   input [DATA_W -1 : 0] dataInP,
   input                 validInP,
   output                enableOutP

   output [DATA_W -1 : 0] dataOutP,
   output                 validOutP,
   input                  enableInP
);

reg [DATA_W -1 : 0] dataInR;
reg [ADDR_W -1 : 0] addrR;
reg                 validOutR;

always @ (posedge clk)
   dataInR <= dataInP;
assign dataOutP <= dataInP;

always @ (posedge clk or negedge aresetNP)
   if (!aresetNP)
      validOutR <= 1'b0;
   else if (!validOutR | enableInP)
      validOutR <= validInP;
assign validOutP <= validOutR;

assign addrOffsetEnableP   <= !sresetNP;
assign addrAbsoluteEnableP <= !sresetNP;

always @ (posedge clk or negedge aresetNP)
   if (aresetNP)
      addrR <= 0;
   else if (sresetNP)
      addrR <= 0;
   else if (addrAbsoluteValidP) begin
      addrR <= addrAbsoluteEnableP;
   else if (addrOffsetValidP)
      addrR <= addrR + addrOffsetP;
   else if (validInP && enableInP)
      addrR <=+ 1;

assign addrP <= addrR;

endmodule
