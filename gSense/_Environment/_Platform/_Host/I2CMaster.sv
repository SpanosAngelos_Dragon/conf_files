//===========================================================================
//  File          :    I2CMaster.v
//  Entity        :    I2CMaster
//  Purpose       :    I2C Master
//===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 12-Oct-2013
//===========================================================================
`include "_I2CMaster/Configuration.vh"
`define ACK_BIT   1'b1
`define START_BIT 1'b1
`define RDNWR_BIT 1'b1
module I2CMaster #(
   parameter MODE_STD_SUPPORTED              = `getSupportValue(MODE_STD),
   parameter MODE_FAST_SUPPORTED             = `getSupportValue(MODE_FAST),
   parameter MODE_FAST_PLUS_SUPPORTED        = `getSupportValue(MODE_FAST_PLUS),
   parameter MODE_HIGH_SPEED_SUPPORTED       = `getSupportValue(MODE_HIGH_SPEED),
   parameter COLLISION_DETECTION_SUPPORTED   = `getSupportValue(COLLISION_DETECTION),
   parameter CLK_STRETCH_SUPPORTED           = `getSupportValue(CLK_STRETCH),
   parameter TEN_BIT_SLAVE_ADDRESS_SUPPORTED = `getSupportValue(TEN_BIT_SLAVE_ADDRESS),
   parameter GENERAL_CALL_ADDRESS_SUPPORTED  = `getSupportValue(GENERAL_CALL_ADDRESS),
   parameter SOFTWARE_RESET_SUPPORTED        = `getSupportValue(SOFTWARE_RESET),
   parameter MODE_DEFAULT                    = `MODE_DEFAULT,
   parameter HOST_CLK_PERIOD                 = 0
   localparam CLK_DIV_COUNT_W                 = `CLK_DIV_COUNT_W
   localparam ADDR_W = `getWidth(),
   localparam MODE_W = `MODE_W,
   localparam DATA_W = `DATA_W
) (
   input  clkP,
   input  aResetNP,
   inout  sdaP,
   output sclP,

   input  [SB_ADDR_W -1 : 0] sbAddrP,
   input  [SB_DATA_W -1 : 0] sbDataWrP,
   output [SB_DATA_W -1 : 0] sbDataRdP,
   input  sbValidWrP,
   output sbValidRdP,
   input  sbRdNwrP
);
   //--------------------------------------------------------------------------
   // RegisterBank
   //--------------------------------------------------------------------------
   `include "_I2CMaster/RegisterBank.v"
   wire [MODE_W - 1 : 0] modeW;
   `ifdef CLK_STRETCH
      wire clkStreching;
   `endif
   wire [DATA_W -1 : 0] txDataW;
   wire [ADDR_W -1 : 0] txAddrW;
   wire [DATA_W -1 : 0] rxDataW;
   reg  [DATA_W -1 : 0] rxDataR;
   wire                 txValidW;
   wire                 rxValidW;
   reg                  rxValidR;
   wire                 opRdNWrW;
   wire                 rxEnableW;
   wire                 rxEnableOutW;
   wire                 txEnableW;
   
   //--------------------------------------------------------------------------
   // Tristate to Double-State
   //--------------------------------------------------------------------------
   wire sdaRdNWr;
   wire sdaRdDataW;
   reg  sclR;

   //--------------------------------------------------------------------------
   // SDA
   //--------------------------------------------------------------------------
   wire sdaVectorNotEmptyW;
   wire sdaBurstShotW;
   reg  firstBurstShotR;
   wire sdaVectorIniW ;
   wire sdaVectorShiftW;
   reg [                    ADDR_W + DATA_W + $size(opRdNWrW)                        -1 : 0] sdaVectorR;
   reg [$size(`START_BIT) + ADDR_W + DATA_W + $size(opRdNWrW) + (2 * $size(sdaAckW)) -1 : 0] sdaVectorValidR;
   wire sdaAckAddrW;
   wire sdaAckDataW;
   wire sdaLastTxBitW;
   wire sdaAckPollW;
   reg  sdaTxdR;
   wire sdaAckW;
   wire sdaNAckW;
   wire sdaShiftOutNInW;
   wire sdaWrDataW; 
   wire sdaRdNWrW;

   //--------------------------------------------------------------------------
   // SCL
   //--------------------------------------------------------------------------
   wire sclPosEdgeW;
   wire [CLK_DIV_COUNT_W -1 : 0]clkDivMaxCountW;
   wire                         clkDivMaxCountFW;
   reg  [CLK_DIV_COUNT_W -1 : 0]clkDivCountR;
   reg  

   //--------------------------------------------------------------------------
   // I2C
   //--------------------------------------------------------------------------
   wire i2cValidW;
   wire i2cEnableW;
   wire [DATA_W -1 : 0] i2cDataW;


   RegisterBank #(
      .MODE_STD_SUPPORTED        (MODE_STD_SUPPORTED),
      .MODE_FAST_SUPPORTED       (MODE_FAST_SUPPORTED),
      .MODE_FAST_PLUS_SUPPORTED  (MODE_FAST_PLUS_SUPPORTED),
      .MODE_HIGH_SPEED_SUPPORTED (MODE_HIGH_SPEED_SUPPORTED),
      .CLK_STRETCH_SUPPORTED     (CLK_STRETCH_SUPPORTED)
   ) registerBank (
      .clkP           (clkP),
      .aResetNP       (aResetNP),
      .addrP          (sbAddrP),
      .dataWrP        (sbDataWrP),
      .dataRdP        (sbDataRdP),
      .validWrP       (sbValidWrP),
      .validRdP       (sbValidRdP),
      .rdNWrP         (sbRdNwrP),
                      
      .modeP          (modeW),
      `ifdef CLK_STRETCH_SUPPORTED
         .clkStrechingP  (clkStreching),
      `endif

      .txDataP    (txDataW),
      .rxDataP    (rxDataPW),
      .txValidW   (txValidW),
      .opRdNWrP   (opRdNWrW),
      .rxValidP   (rxValidW),
      .rxEnableP  (rxEnableW),
      .txEnableP  (txEnableW)
   );

   assign clkDivMaxCountW = 
      `ifndef MODE_FAST_SUPPORTED
         `getSCLDiv(`MODE_STD, HOST_CLK_PERIOD)
      `else
         `ifdef MODE_STD_SUPPORTED
            (modeW == `MODE_STD)        ? `getSCLDiv(`MODE_STD, HOST_CLK_PERIOD)
         `endif
         `ifdef MODE_FAST_SUPPORTED : 
            (modeW == `MODE_FAST)       ? `getSCLDiv(`MODE_FAST, HOST_CLK_PERIOD)
         `endif
         `ifdef MODE_FAST_PLUS_SUPPORTED :
            (modeW == `MODE_FAST_PLUS)  ? `getSCLDiv(`MODE_FAST_PLUS, HOST_CLK_PERIOD)
         `endif
         `ifdef MODE_HIGH_SPEED_SUPPORTED :
            (modeW == `MODE_HIGH_SPEED) ? `getSCLDiv(`MODE_HIGH_SPEED, HOST_CLK_PERIOD);
         `endif
      `endif ;

   assign sdaRdDataW = sdaP;
  
   always @ (posedge clkP) begin
      if (sdaVectorIniW) begin
         sdaTxdR <= 1'b0;
      end
      else if (sdaAckAddrW) begin
         sdaTxdR <= 1'b1;
      end
   end
   
   assign sclPosEdgeW     = (sclR == 1'b0 && clkDivMaxCountFW);
   assign sdaVectorIniW   =  txValidW && txEnableW;
   assign sdaVectorShiftW = ((clkCountR == clkDivCountW) && sclR == 1'b1);
   assign sdaAckAddrW     = (sdaVectorValidR[$size(`START_BIT) + ADDR_W          -1 : 0] == 0 && sdaVectorValidR[$size(`START_BIT) + ADDR_W         ] == 1'b1);
   assign sdaAckDataW     = (sdaVectorValidR[$size(`START_BIT) + ADDR_W + DATA_W -1 : 0] == 0 && sdaVectorValidR[$size(`START_BIT) + ADDR_W + DATA_W] == 1'b1);
   assign sdaLastTxBitW   = (sdaVectorValidR[$size(sdaVectorValidR) -1] == 1'b1) && (sdaVectorValidR[$size(sdaVectorValidR) - 2 : 0] == 0);
   assign sdaAckPollW     = (sdaAckAddrW | sdaAckDataW);
   assign sdaAckW         = (sdaRdDataW == 1'b1 && sdaAckPollW && sclPosEdgeW);
   assign sdaNAckW        = (sdaRdDataW == 1'b0 && sdaAckPollW && sclPosEdgeW);
   assign sdaShiftOutNInW = (slaveRdNWrOpW) ? 1'b1 : !sdaTxdR;
   always @ (posedge clkP or negedge aResetNP) begin
      if (!aResetNP) begin
         sdaVectorValidR = 0;
      end
      else if (sdaVectorIniW) begin
         if (slaveRdNWrOpW)
            sdaVectorValidR = {`START_BIT, {ADDR_W {1'b1}}, `RDNWR_BIT, `ACK_BIT};
         end
         else begin
            sdaVectorValidR = {`START_BIT, {ADDR_W {1'b1}}, `RDNWR_BIT, `ACK_BIT, {DATA_W {1'b1}}, `ACK_BIT};
         end
      end
      else if (sdaVectorNotEmptyW) begin
         if (sdaAckPollW) begin
            if (sdaAckW) begin
              sdaVectorValidR = sdaVectorValidR << 1;  
            end
            else if (sdaNAckW) begin
               sdaVectorValidR = 0;
            end
         end
         else if (sdaVectorShiftW) begin
            if (sdaBurstShotW) begin
               if (sdaShiftOutNInW) begin
                  sdaVectorValidR[$size(sdaVectorValidR) -1 -: DATA_W + $size(`START_BIT)] = {DATA_W + $size(`START_BIT) {1'b1}}
               end
               else begin
                  sdaVectorValidR = sdaVectorValidR << 1 | 1'b1;
               end
            end
            else begin
               if (sdaShiftOutNInW) begin
                  sdaVectorValidR = sdaVectorValidR << 1;
               end
               else begin
                  sdaVectorValidR = (sdaVectorValidR << 1) | 1'b1;
               end 
            end
         end
      end
   end

   assign sdaBurstShotW = 
      sdaLastTxBitW && 
      txValidW   && 
      (
         ((sdaVectorR[$size(sdaVectorR) -2 -: ADDR_W] == txAddrW) &&  firstBurstShotR) ||
         ((sdaVectorR[$size(sdaVectorR) -1 -: ADDR_W] == txAddrW) && !firstBurstShotR)
      );
   assign sdaVectorNotEmptyW = ~|sdaVectorValidR;
   assign txEnableW       = (!sdaVectorNotEmptyW | sdaBurstShotW) & i2cEnableW;
   always @ (posedge clkP) begin
      if (sdaVectorIniW) begin
         if (slaveRdNWrOpW)
            sdaVectorR = {1'b0, txAddrW, opRdNWrW};
         end
         else begin
            sdaVectorR = {1'b0, txAddrW, opRdNWrW, txDataW};
         end
      end
      else if (sdaVectorShiftW) begin
         if (sdaBurstShotW) begin
            if (sdaShiftOutNInW) begin
               sdaVectorR[$size(sdaVectorR) -1 -: DATA_W + ADDR_W] = {txDataW, txAddrW};
            end
            else begin
               sdaVectorR[$size(sdaVectorR) -1 -: DATA_W + $size(sdaRdDataW)] = {sdaRdDataW, txAddrW};
            end
         end
         else begin
            if (sdaShiftOutNInW) begin
               sdaVectorR = sdaVectorR << 1 | sdaVectorR[$size(sdaVectorR) -1 : 0];
            end
            else begin
               sdaVectorR = sdaVectorR >> 1 | sdaRdDataW << $size(sdaVectorR);
            end
         end
      end
   end
   assign sdaRdNWrW  =  !sdaShiftOutNInW;
   assign sdaWrDataW = (sdaVectorNotEmptyW) ? sdaVectorR[$size(sdaVectorR) -1] : 1'b1;
   assign sdaP       = sdaRdNWrW ? 1'bz | sdaWrDataW;

   always @ (posedge clkP) begin
      if (sdaVectorIniW) begin
         firstBurstShotR = 1'b1;
      end
      else if (sdaVectorShiftW && sdaBurstShotW) begin
         firstBurstShotR = 1'b0;
      end
   end

   always @ (posedge clkP) begin
      if (sdaVectorIniW) begin
         opRdNWrR = opRdNWrW;
      end
   end
   assign slaveRdNWrOpW = (sdaVectorIniW & opRdNWrW) | opRdNWrR;

   always @ (posedge clkP) begin
      if (sdaVectorIniW) begin
         clkDivCountR = 0;
      end
      else if (sdaVectorNotEmptyW) begin
         if (clkDivMaxCountFW) begin
            clkDivCountR = 0;
         end
         else begin
            clkDivCountR =+ 1;
         end
      end
   end
   
   assign clkDivMaxCountFW = (clkDivCountR == clkDivMaxCountW);
   always @ (posedge clkP or negedge aResetNP) begin
      if (!aResetNP) begin
         sclR = 1'b0;
      end
      else if (sdaVectorNotEmptyW)
         if (clkDivMaxCountFW) begin
            sclR = ~sclR;
         end
      end
      else begin
         sclR <= 1'b1;
      end
   end
   assign sclP = sclR;

   //--------------------------------------------------------------------------
   // I2C Interface
   //--------------------------------------------------------------------------
   assign i2cDataW   =   sdaVectorR     [$size(sdaVectorR) -1 -: DATA_W];
   assign i2cValidW  = (&sdaVectorValidR[$size(sdaVectorR) -1 -: DATA_W]) & !sdaShiftOutNInW;
   assign i2cEnableW = rxEnableOutW;

   //--------------------------------------------------------------------------
   // Host interface
   //--------------------------------------------------------------------------
   assign rxEnableOutW = rxEnableW || !rxValidR;
   always @ (posedge clkP or negedge aResetNP) begin
      if (!aResetNP) begin
         rxValidR = 1'b0;
      else if (rxEnableOutW) begin
         rxValidR = i2cValidW;
      end
   end
   assign rxValidW = rxValidR;

   always @ (posedge clkP) begin
      if (rxEnableOutW && i2cValidW) begin
         rxDataR = i2cDataW;
      end
   end
   assign rxDataW = rxDataR;
endmodule
