// ===========================================================================
//  File          :    Platform.v
//  Entity        :    Platform
//  Purpose       :    Test platform
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 10-Oct-2013
// ===========================================================================
module Platform # (
   parameter HOST_PRG_ADDR_W = 16,
   parameter HOST_PRG_DATA_W = 31,
   parameter SENSOR_PRG_ADDR_W = 16,
   parameter SENSOR_PRG_DATA_W = 31
) (
   input clkP,
   input aResetNP
   `ifdef HOST_PRG_ROM_EXT,
      output [HOST_PRG_ADDR_W -1 : 0 ] hostAddrP,
      input  [HOST_PRG_DATA_W -1 : 0 ] hostDataP
   `endif
   `ifdef SENSOR_PRG_ROM_EXT,
      output [SENSOR_PRG_ADDR_W -1 : 0 ] sensorAddrP,
      input  [SENSOR_PRG_DATA_W -1 : 0 ] sensorDataP
   `endif
);
   wire sclP;
   wire sdaP;

   Host #(
      .PRG_ADDR_W    (HOST_PRG_ADDR_W),
      .PRG_DATA_W    (HOST_PRG_DATA_W)
   ) host (
      .clkP     (clkP),
      .aResetNP (aResetNP),
      .sdaP     (sda),
      .sclP     (scl)
      `ifdef HOST_PRG_ROM_EXT,
         .addrP (addrP),
         .dataP (dataP)
      `endif
   );

   Sensor sensor (
      .clkP       (clkP),
      .aResetNP   (aResetNP),
      .sdaP       (sdaP),
      .sclP       (sclP)
      `ifdef SENSOR_PRG_EXT,
         .addrP   (addrP),
         .dataP   (dataP)
      `endif
   );
endmodule
