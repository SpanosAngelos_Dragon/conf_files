/*! 
 * \file      ipXact.h
 * \brief     ipXact
 * \details   Definitions file for ipXact.
 * \author    Angelos Spanos
 * \version   1.0
 * \date      13-Jul-2013
 * \copyright GNU Public License.
 */

/// Error Flags.
/// IO errors
#define ERR__IO__BAD_IF -1 //< Bad ifstream pointer.
#define ERR__FORMAT__IDENTIFIER (ERR__FORMAT__IDENTIFIER - 1)

/// Debug switch
#define DEBUG__SWITCH_MASTER                     true
#define DEBUG__SWITCH_XML                       (true && DEBUG__SWITCH_MASTER)
#define DEBUG__SWITCH_XMLSection                (true && DEBUG__SWITCH_MASTER)
#define DEBUG__SWITCH_XMLSectionTitle           (true && DEBUG__SWITCH_MASTER)
#define DEBUG__SWITCH_XMLSectionTitleIDentifier (true && DEBUG__SWITCH_MASTER)

/// Expected constants
#define MAX__XML_STRING__LENGTH 256
#define MAX_XML_TITLE_IDENTIFIER_NUM 256
