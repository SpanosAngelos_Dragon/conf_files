// ===========================================================================
//  File          :    Environment.v
//  Entity        :    Environment
//  Purpose       :    Platform enviroment emulator
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 10-Oct-2013
// ===========================================================================
`include "_Environment/Environment.vh"

module Environment #(
   parameter INITIAL_CLOCK_HOLD_Z_T = `INITIAL_CLOCK_HOLD_Z_T,
   parameter INITIAL_CLOCK_HOLD_0_T = `INITIAL_CLOCK_HOLD_0_T,
   parameter INITIAL_RESET_HOLD_Z_T = `INITIAL_RESET_HOLD_Z_T,
   parameter INITIAL_RESET_HOLD_0_T = `INITIAL_RESET_HOLD_0_T,
   parameter CLOCK_PERIOD           = `CLOCK_PERIOD,

   parameter HOST_PRG_EXT    = `HOST_PRG_EXT,
   parameter HOST_PRG_DATA_W = `HOST_PRG_DATA_W,
   parameter HOST_PRG_ADDR_W = `HOST_PRG_ADDR_W
) ();
   reg clk;
   reg aResetN;
   `ifdef HOST_PRG_EXT
      wire [HOST_PRG_ADDR_W -1 : 0] addr;
      wire [HOST_PRG_DATA_W -1 : 0] data;
   `endif

   initial aResetNInitialBlock : begin
      clk     = 1'bz;
      #INITIAL_CLOCK_HOLD_Z_T;
      clk     = 1'b0;
      #INITIAL_CLOCK_HOLD_0_T;
   end
   
   initial begin
      aResetN = 1'bz;
      #INITIAL_RESET_HOLD_Z_T;
      aResetN = 1'b0;
      #INITIAL_RESET_HOLD_0_T;
      aResetN = 1'b1;
   end
   
   always begin
      #CLOCK_PERIOD;
      clk = !clk;
   end

   Platform # (
      .HOST_PRG_DATA_W (HOST_PRG_DATA_W),
      .HOST_PRG_ADDR_W (HOST_PRG_ADDR_W)
   ) platform (
      .clkP       (clk),
      .aResetNP   (aResetN)
      `ifdef HOST_PRG_EXT,
         .addrP   (addr),
         .dataP   (data)
      `endif
   );

   `ifdef HOST_PRG_EXT
      ROM # (
         .DATA_W  (HOST_PRG_DATA_W),
         .ADDR_W  (HOST_PRG_ADDR_W),
         .DATA_P  (HOST_PRG_DATA_F)
      ) host_prg_rom_ext (
         .clkP  (clk),
         .addrP (addr),
         .dataP (data)
      );
   `endif

   `ifdef SENSOR_PRG_EXT
      ROM # (
         .DATA_W  (SENSOR_PRG_DATA_W),
         .ADDR_W  (SENSOR_PRG_ADDR_W),
         .DATA_P  (SENSOR_PRG_DATA_F)
      ) sensor_prg_rom_ext (
         .clkP  (clk),
         .addrP (addr),
         .dataP (data)
      );
   `endif

endmodule
